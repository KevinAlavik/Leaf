MAKEFLAGS += --no-print-directory

CONF_DIR := conf
BIN_DIR := bin
DEPS_DIR := deps
ISO_DIR := iso_root

ISO := Leaf.iso

UEFI_BIOS := bios/ovmf.fd
PROC := 2

SOURCES := kernel
DEPS := limine=https://github.com/limine-bootloader/limine/archive/refs/heads/v7.x-binary.zip simpterm=https://github.com/roidsos/simpterm/archive/refs/heads/master.zip

DEPS_NAME := $(foreach dep,$(DEPS),$(firstword $(subst =, ,$(dep))))
DEPS_URL := $(foreach dep,$(DEPS),$(word 2,$(subst =, ,$(dep))))

.PHONY: all clean setup deps $(ISO)

all: setup deps $(SOURCES) $(ISO)

setup:
	@mkdir -p $(BIN_DIR) $(DEPS_DIR) $(ISO_DIR)/boot/limine $(ISO_DIR)/EFI/BOOT

define DOWNLOAD_AND_EXTRACT
$(DEPS_DIR)/$(1):
	@echo ">>> Downloading dependency: \"$(1)\" from \"$(2)\""
	@wget -q $(2) -O $(DEPS_DIR)/temp.zip || { echo "Failed to download $(1)"; exit 1; }
	@unzip -o $(DEPS_DIR)/temp.zip -d $(DEPS_DIR) > /dev/null || { echo "Failed to unzip $(1)"; exit 1; }
	@rm $(DEPS_DIR)/temp.zip
	@mv $(DEPS_DIR)/$(1)-* $(DEPS_DIR)/$(1)
endef

$(foreach dep,$(DEPS),$(eval $(call DOWNLOAD_AND_EXTRACT,$(firstword $(subst =, ,$(dep))),$(word 2,$(subst =, ,$(dep))))))

deps: $(foreach dep,$(DEPS_NAME),$(DEPS_DIR)/$(dep))

$(SOURCES): deps
	@echo ">>> Building target: \"$@\""
	@find $@ -type f \( -iname \*.c -o -iname \*.h \) -print0 | xargs -0 clang-format -i
	@$(MAKE) -C $@ || { echo "!!! Failed to build $@"; exit 1; }
	@cp $@/$(BIN_DIR)/* $(BIN_DIR)/ > /dev/null 2>&1
	@rm -rf $@/$(BIN_DIR) > /dev/null 2>&1
	@$(MAKE) -C $@ clean

$(ISO): setup $(ISO_DIR)/boot/limine/limine.cfg $(ISO_DIR)/boot/limine/limine-bios.sys $(ISO_DIR)/boot/limine/limine-bios-cd.bin $(ISO_DIR)/boot/limine/limine-uefi-cd.bin $(ISO_DIR)/EFI/BOOT/BOOTX64.EFI $(ISO_DIR)/EFI/BOOT/BOOTIA32.EFI
	@echo ">>> Creating bootable ISO: $(ISO)"
	@cp $(BIN_DIR)/* $(ISO_DIR)/
	@xorriso -as mkisofs -b boot/limine/limine-bios-cd.bin \
		-no-emul-boot -boot-load-size 4 -boot-info-table \
		--efi-boot boot/limine/limine-uefi-cd.bin \
		-efi-boot-part --efi-boot-image --protective-msdos-label \
		$(ISO_DIR) -o $(ISO) > /dev/null 2>&1
	@./$(DEPS_DIR)/limine/limine bios-install $(ISO) > /dev/null 2>&1
	@echo ">>> Finished."

$(ISO_DIR)/boot/limine/limine.cfg: $(CONF_DIR)/limine.cfg
	@cp $< $@

$(ISO_DIR)/boot/limine/limine-bios.sys: $(DEPS_DIR)/limine/limine-bios.sys
	@cp $< $@

$(ISO_DIR)/boot/limine/limine-bios-cd.bin: $(DEPS_DIR)/limine/limine-bios-cd.bin
	@cp $< $@

$(ISO_DIR)/boot/limine/limine-uefi-cd.bin: $(DEPS_DIR)/limine/limine-uefi-cd.bin
	@cp $< $@

$(ISO_DIR)/EFI/BOOT/BOOTX64.EFI: $(DEPS_DIR)/limine/BOOTX64.EFI
	@cp $< $@

$(ISO_DIR)/EFI/BOOT/BOOTIA32.EFI: $(DEPS_DIR)/limine/BOOTIA32.EFI
	@cp $< $@

clean:
	@rm -rf $(BIN_DIR) $(DEPS_DIR) $(ISO_DIR) $(ISO)
	@$(MAKE) -C kernel clean
