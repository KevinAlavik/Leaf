# Leaf

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/5980a18bed0b48e181a7bf1dde2a6944)](https://app.codacy.com/gl/KevinAlavik/Leaf/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)

Leaf is an operating system designed for the x86_64 architecture, with a focus on being "Light as a Leaf," hence the name.

Leaf does not adhere to any *NIX standards. Instead, it features unique designs and implementations tailored by our team.

![Leaf showcase](https://gitlab.com/KevinAlavik/Leaf/-/raw/master/public/showcase/main.png)

## Building and Running

Leaf employs a custom build tool that interfaces with the primary build system (using make). This tool simplifies the process of building and running the OS, providing multiple commands for various tasks.

![Leaf build showcase](https://gitlab.com/KevinAlavik/Leaf/-/raw/master/public/showcase/build.png)

### Prerequisites

Before you begin, ensure that you have a Unix-like operating system as the CLI tool is only compatible with such environments.

### Building the CLI

To build the CLI, execute the following command:

```bash
make -C build
```

This step compiles the CLI, which uses a custom configuration format for defining build parameters. Hot reloading is enabled by default.

### Launching the CLI

Once the CLI is built, launch it with the following command:

```bash
./build/bin/leaf_build
```

You can then run `help` within the CLI to view a list of available commands.

## Contributing

We welcome contributions from the community. For guidelines on contributing, please refer to our [CONTRIBUTING.md](https://gitlab.com/KevinAlavik/Leaf/-/blob/master/CONTRIBUTING.md) document.

## License

Leaf is licensed under the MIT License. For more details, see the [LICENSE](https://gitlab.com/KevinAlavik/Leaf/-/blob/master/LICENSE) file.