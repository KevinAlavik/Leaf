# Contributing to Leaf

Thank you for your interest in contributing to Leaf! We value and appreciate all contributions. To ensure a smooth process, please adhere to the guidelines outlined below.

## Table of Contents

- [Contributing to Leaf](#contributing-to-leaf)
  - [Table of Contents](#table-of-contents)
  - [Code of Conduct](#code-of-conduct)
  - [How Can I Contribute?](#how-can-i-contribute)
    - [Reporting Bugs](#reporting-bugs)
    - [Suggesting Enhancements](#suggesting-enhancements)
    - [Submitting Pull Requests](#submitting-pull-requests)
  - [Development Setup](#development-setup)
  - [Style Guides](#style-guides)
    - [Git Commit Messages](#git-commit-messages)
    - [Coding Standards](#coding-standards)
  - [License](#license)

## Code of Conduct

Please review and adhere to the [Contributor Covenant Code of Conduct](CODE_OF_CONDUCT.md) when participating in this project.

## How Can I Contribute?

### Reporting Bugs

If you encounter a bug, please create an [issue](https://gitlab.com/KevinAlavik/Leaf/-/issues) with the following details:

- A clear and descriptive title.
- A detailed description of the problem.
- Steps to reproduce the issue.
- Relevant logs or screenshots.
- Your operating system and version.

### Suggesting Enhancements

We welcome suggestions for new features and improvements. To propose an enhancement, create an [issue](https://gitlab.com/KevinAlavik/Leaf/-/issues) with:

- A clear and descriptive title.
- A detailed description of the proposed enhancement.
- Any related code or examples if applicable.

### Submitting Pull Requests

Before starting work on a new feature or fix, ensure there is an open issue describing the problem or enhancement. This helps avoid duplication of effort. To submit a pull request:

1. Fork the repository and create a branch from `main`.
2. Make changes in a clear and modular manner.
3. Ensure your code follows our [coding standards](#coding-standards).
4. Thoroughly test your changes.
5. Create a pull request with a descriptive title and link to the relevant issue.

## Development Setup

Follow these steps to set up your development environment:

1. Clone the repository:
    ```bash
    git clone https://gitlab.com/KevinAlavik/Leaf.git
    cd Leaf
    ```

2. Install required dependencies:
    - x86_64-elf-gcc & x86_64-elf-binutils (or another toolchain)

3. Build and run the builder:
    ```bash
    make -C build; ./build/bin/leaf_build
    ```

4. Set the compiler, linker, and assembler:
    ```bash
    cc <compiler>
    ld <linker>
    as <assembler>
    ```
   (Defaults to x86_64-elf-gcc, x86_64-elf-ld from x86_64-elf-binutils, and nasm).

5. Test the OS:
    ```bash
    run
    ```

## Style Guides

### Git Commit Messages

- Follow the [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/#specification) specification.

### Coding Standards

- Format code using [clang-format](https://clang.llvm.org/docs/ClangFormat.html) with our specified `.clang-format` file.
- Adhere to the style seen in `kernel/include/dev/serial.h` and `kernel/src/dev/serial.c`.
- Ensure thorough comments for complex functions.

## License

By contributing to Leaf, you agree that your contributions will be licensed under the MIT License.

---

Thank you for contributing to Leaf! If you have any questions, please feel free to ask in the issues or contact the maintainers directly.