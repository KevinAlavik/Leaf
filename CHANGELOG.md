# Changelog

All notable changes to Leaf will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and Leaf adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2024-07-05]

### Added

- Copyright for default font.

### Changed

- Made verbose add the `--no-silent` option to build command (make ... --no-silent).

### Fixed

- ...


## [2024-06-20]

### Added

- RSDT / XSDT.
- MADT.
- Started on APIC (IOAPIC and LAPIC).
- MEMORY and CORES to build config, for emulation.
- Finished IOAPIC

### Changed

- Heap now has a smaller init size. Definied in build.cfg.

### Fixed

- Heap checking, avoiding overflows and whatnot.
- Build system crashing on some systems.

## [2024-06-19]

### Added

- VMM Pagemaps / Contexts (vmmCtx).
- Added a simple heap.
- Kernel Malloc.
- ctype posix library.

### Changed

- VMM Usage.
- Expanded <lib/posix/string.h>

### Fixed

- Some code issues (code style).
- PMM issues when freeing.

## [2024-06-18]

### Added

- Proper CC_DEFINES in config file.
- Hot reloading for config files in the build system.
- Macros to build config.
- Started on VMM.

### Changed

- The way you define things in the config file.
- Added %define macro to build config.

### Fixed

- The build system issues.
- Made defining CC_DEFINES safer.

## [2024-06-17]

### Added

- Register dumping on exceptions.
- Simple bitmap PMM.
- `lib/posix/string`: A UNIX-like (POSIX) string.h library.
- DbgoutPrintf to print to COM1.
- Fancy IDT register dump with colors.
- POSIX style assert library.
- Custom type library.
- run_uefi to run with UEFI firmware.

### Changed

- Moved StdoutPrintf to `lib/std/stdout` to have better organization.
- Improved build system to read the default config from `build.conf`.

### Fixed

- Fixed printf implementation, significantly improving it.

## [2024-06-16]

### Added

- Simple GDT and IDT (no IRQ support due to lack of PIC or APIC support).
- Started on custom printf implementation.

### Changed

- Removed wrapper functions for simpterm printing; printf handles this now.

### Fixed

- Fixed terminal font by switching to a better one.

## [2024-06-15]

### Added

- Tool to generate C array from binary input (file -> static uint8_t).

### Changed

- Moved to [simpterm](https://github.com/roidsos/simpterm).

### Fixed

- (No notable fixes documented).

## [2024-06-13]

### Added

- Helper functions to print numbers as strings to serial (temporarily).

### Changed

- Terminal scrolling functionality was broken.

### Fixed

- Improved terminal speed.

## [2024-06-12]

### Added

- Changelog file.
- `public/index.html` and support for GitLab Pages.
- Started on GDT.

### Changed

- Updated README to be more helpful.
- Migrated from text README to markdown README.
- Began a proper rewrite of `docs/DESIGN.md`, adding project and file structure.

### Fixed

- Terminal defines for the kernel.
- Project structure.

## [2024-06-11]

### Added

- Simple terminal based on <https://github.com/aurixos/nex>.

### Changed

- Made `framebufferPointer` available through `kernel.h`.
- Removed most of the bootlog, which will return soon.

### Fixed

- Adjusted build system to avoid using the `c2x` standard, resolving build issues on Linux.