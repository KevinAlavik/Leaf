#include "command.h"
#include "ansi.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_COMMANDS 100

Command all_commands[MAX_COMMANDS];
int num_commands = 0;

int register_command(const char *command, CommandHandler handler)
{
	if (num_commands >= MAX_COMMANDS) {
		printf("[ " ANSI_COLOR_RED "ERROR" ANSI_COLOR_RESET
			   " ] Cannot register more commands, maximum reached.\n");
		return -1;
	}

	all_commands[num_commands].command = command;
	all_commands[num_commands].handler = handler;
	num_commands++;

	return 0;
}

int deregister_command(const char *command)
{
	int found = 0;
	for (int i = 0; i < num_commands; i++) {
		if (strcmp(command, all_commands[i].command) == 0) {
			found = 1;
			for (int j = i; j < num_commands - 1; j++) {
				all_commands[j] = all_commands[j + 1];
			}
			num_commands--;
			break;
		}
	}

	if (!found) {
		printf("[ " ANSI_COLOR_RED "ERROR" ANSI_COLOR_RESET
			   " ] Command '%s' not found.\n",
			   command);
		return -1;
	}

	return 0;
}
