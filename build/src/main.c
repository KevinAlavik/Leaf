#include "command.h"
#include "ansi.h"

#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <ctype.h>
#include <signal.h>
#include <sys/stat.h>

#define MAX_COMMAND_LENGTH 4096
#define MAX_MACROS 100
#define MAX_NAME_LENGTH 1000
#define MAX_ARGUMENTS 10
#define MAX_DEFINES 10

#define CONFIG_FILE "conf/build.cfg"
#define POLLING_INTERVAL 1

#define SPECIAL_PREFIX '%'

int verbose = 0;
int first_time = 1;
int build_status;

char *CC;
char *LD;
char *AS;

char *CC_FLAGS;
char *LD_FLAGS;
char *AS_FLAGS;

char *MEMORY;
char *CORES;

typedef struct {
	char *name;
	char *value;
} Define;

typedef struct {
	char *name;
	char *value;
} Macro;

Define defines[MAX_DEFINES] = { "" };
Macro macros[MAX_MACROS] = { "" };
int define_count = 0;
int macro_count = 0;

void add_macro(const char *name, const char *value)
{
	if (macro_count >= MAX_MACROS) {
		printf("Error: Maximum number of macros reached.\n");
		return;
	}

	macros[macro_count].name = strdup(name);
	macros[macro_count].value = strdup(value);

	if (macros[macro_count].name == NULL || macros[macro_count].value == NULL) {
		printf("Error: Memory allocation failed.\n");
		if (macros[macro_count].name != NULL)
			free(macros[macro_count].name);
		if (macros[macro_count].value != NULL)
			free(macros[macro_count].value);
		return;
	}

	macro_count++;
}

char *get_macro_value(const char *name)
{
	for (int i = 0; i < macro_count; i++) {
		if (strcmp(macros[i].name, name) == 0) {
			return strdup(macros[i].value);
		}
	}

	return NULL;
}

void clear_macros()
{
	for (size_t i = 0; i < macro_count; i++) {
		free(macros[i].name);
		free(macros[i].value);
		macros[i].name = NULL;
		macros[i].value = NULL;
	}
	macro_count = 0;
}

void clear_defines()
{
	for (size_t i = 0; i < define_count; i++) {
		free(defines[i].name);
		free(defines[i].value);
		defines[i].name = NULL;
		defines[i].value = NULL;
	}
	define_count = 0;
}

int execute_command(char *command_line);

int set_define(int argc, char **argv)
{
	if (argc < 3) {
		printf(
			"[ %sERROR%s ] \"define\" Expects two arguments, Usage: \"define <name> <value>\"\n",
			ANSI_COLOR_RED, ANSI_COLOR_RESET);
		return 1;
	}

	int i;
	for (i = 0; i < MAX_DEFINES; i++) {
		if (defines[i].name == NULL || defines[i].value == NULL) {
			defines[i].name = malloc(strlen(argv[1]) + 1);
			defines[i].value = malloc(strlen(argv[2]) + 1);
			if (defines[i].name == NULL || defines[i].value == NULL) {
				printf("[ %sERROR%s ] Memory allocation error.\n",
					   ANSI_COLOR_RED, ANSI_COLOR_RESET);
				return 1;
			}
			strcpy(defines[i].name, argv[1]);
			strcpy(defines[i].value, argv[2]);
			break;
		}
	}

	if (i == MAX_DEFINES) {
		printf("[ %sERROR%s ] Maximum number of defines exceeded.\n",
			   ANSI_COLOR_RED, ANSI_COLOR_RESET);
		return 1;
	}

	if (verbose)
		printf("Defined: %s=%s\n", defines[i].name, defines[i].value);
	return 0;
}

int set_verbose(int argc, char **argv)
{
	verbose = !verbose;
	return 0;
}

int clear_screen(int argc, char **argv)
{
	printf("\033[2J\033[1;1H");
	printf("%sLeaf Build Tool v%d.%d.%d (Use %shelp%s to get started)%s\n",
		   ANSI_COLOR_BRIGHT_GREEN, VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH,
		   ANSI_COLOR_CYAN, ANSI_COLOR_BRIGHT_GREEN, ANSI_COLOR_RESET);
	return 0;
}

int set_cc(int argc, char **argv)
{
	if (argc < 2) {
		printf(
			"[ %sERROR%s ] \"cc\" Expects an argument, Usage: \"cc <c compiler>\"\n",
			ANSI_COLOR_RED, ANSI_COLOR_RESET);
		return 1;
	}

	char *new_CC = malloc(strlen(argv[1]) + 1);
	if (new_CC == NULL) {
		printf("[ %sERROR%s ] Memory allocation error.\n", ANSI_COLOR_RED,
			   ANSI_COLOR_RESET);
		return 1;
	}

	strcpy(new_CC, argv[1]);

	if (CC != NULL) {
		free(CC);
	}

	CC = new_CC;
	if (verbose)
		printf("CC set to: %s\n", CC);
	return 0;
}

int set_ld(int argc, char **argv)
{
	if (argc < 2) {
		printf(
			"[ %sERROR%s ] \"cc\" Expects an argument, Usage: \"ld <linker>\"\n",
			ANSI_COLOR_RED, ANSI_COLOR_RESET);
		return 1;
	}

	char *new_LD = malloc(strlen(argv[1]) + 1);
	if (new_LD == NULL) {
		printf("[ %sERROR%s ] Memory allocation error.\n", ANSI_COLOR_RED,
			   ANSI_COLOR_RESET);
		return 1;
	}

	strcpy(new_LD, argv[1]);

	if (LD != NULL) {
		free(LD);
	}

	LD = new_LD;
	if (verbose)
		printf("LD set to: %s\n", LD);
	return 0;
}

int set_as(int argc, char **argv)
{
	if (argc < 2) {
		printf(
			"[ %sERROR%s ] \"as\" Expects an argument, Usage: \"as <assembler>\"\n",
			ANSI_COLOR_RED, ANSI_COLOR_RESET);
		return 1;
	}

	char *new_AS = malloc(strlen(argv[1]) + 1);
	if (new_AS == NULL) {
		printf("[ %sERROR%s ] Memory allocation error.\n", ANSI_COLOR_RED,
			   ANSI_COLOR_RESET);
		return 1;
	}

	strcpy(new_AS, argv[1]);

	if (AS != NULL) {
		free(AS);
	}

	AS = new_AS;
	if (verbose)
		printf("AS set to: %s\n", AS);
	return 0;
}

int set_cc_flags(int argc, char **argv)
{
	if (argc < 2) {
		printf(
			"[ %sERROR%s ] \"cc_flags\" Expects an argument, Usage: \"cc_flags <flags>\"\n",
			ANSI_COLOR_RED, ANSI_COLOR_RESET);
		return 1;
	}

	char *new_CC_FLAGS = malloc(strlen(argv[1]) + 1);
	if (new_CC_FLAGS == NULL) {
		printf("[ %sERROR%s ] Memory allocation error.\n", ANSI_COLOR_RED,
			   ANSI_COLOR_RESET);
		return 1;
	}

	strcpy(new_CC_FLAGS, argv[1]);

	if (CC_FLAGS != NULL) {
		free(CC_FLAGS);
	}

	CC_FLAGS = new_CC_FLAGS;
	if (verbose)
		printf("CC_FLAGS set to: %s\n", CC_FLAGS);
	return 0;
}

int set_ld_flags(int argc, char **argv)
{
	if (argc < 2) {
		printf(
			"[ %sERROR%s ] \"ld_flags\" Expects an argument, Usage: \"ld_flags <flags>\"\n",
			ANSI_COLOR_RED, ANSI_COLOR_RESET);
		return 1;
	}

	char *new_LD_FLAGS = malloc(strlen(argv[1]) + 1);
	if (new_LD_FLAGS == NULL) {
		printf("[ %sERROR%s ] Memory allocation error.\n", ANSI_COLOR_RED,
			   ANSI_COLOR_RESET);
		return 1;
	}

	strcpy(new_LD_FLAGS, argv[1]);

	if (LD_FLAGS != NULL) {
		free(LD_FLAGS);
	}

	LD_FLAGS = new_LD_FLAGS;
	if (verbose)
		printf("LD_FLAGS set to: %s\n", LD_FLAGS);
	return 0;
}

int set_as_flags(int argc, char **argv)
{
	if (argc < 2) {
		printf(
			"[ %sERROR%s ] \"as_flags\" Expects an argument, Usage: \"as_flags <flags>\"\n",
			ANSI_COLOR_RED, ANSI_COLOR_RESET);
		return 1;
	}

	char *new_AS_FLAGS = malloc(strlen(argv[1]) + 1);
	if (new_AS_FLAGS == NULL) {
		printf("[ %sERROR%s ] Memory allocation error.\n", ANSI_COLOR_RED,
			   ANSI_COLOR_RESET);
		return 1;
	}

	strcpy(new_AS_FLAGS, argv[1]);

	if (AS_FLAGS != NULL) {
		free(AS_FLAGS);
	}

	AS_FLAGS = new_AS_FLAGS;
	if (verbose)
		printf("AS_FLAGS set to: %s\n", AS_FLAGS);
	return 0;
}

void trim(char *str)
{
	char *start = str;
	while (*start && (*start == ' ' || *start == '\t' || *start == '\n')) {
		start++;
	}

	char *end = str + strlen(str) - 1;
	while (end > start && (*end == ' ' || *end == '\t' || *end == '\n')) {
		end--;
	}
	*(end + 1) = '\0';

	if (end > start) {
		memmove(str, start, end - start + 2);
	} else {
		*str = '\0';
	}
}

int load_conf(int argc, char **argv)
{
	if (argc < 2) {
		printf(
			"[ %sERROR%s ] \"load\" expects an argument. Usage: \"load <configuration file>\"\n",
			ANSI_COLOR_RED, ANSI_COLOR_RESET);
		return 1;
	}

	FILE *conf_file = fopen(argv[1], "r");
	if (conf_file == NULL) {
		printf("[ %sERROR%s ] Cant open \"%s\"!\n", ANSI_COLOR_RED,
			   ANSI_COLOR_RESET, argv[1]);
		return 1;
	}

	char line[MAX_COMMAND_LENGTH];
	int cc_defines = 0;
	int cc_defines_started = 0;

	char special_id[100];
	int special = 0;

	clear_macros();
	clear_defines();

	while (fgets(line, MAX_COMMAND_LENGTH, conf_file) != NULL) {
		char parse_line[MAX_COMMAND_LENGTH];
		strcpy(parse_line, line);

		if (cc_defines_started && line[0] == ']') {
			cc_defines = 0;
			cc_defines_started = 0;
		}

		if (parse_line[0] == SPECIAL_PREFIX) {
			special = 1;
		} else {
			special = 0;
		}

		int i = 0;

		if (special) {
			i = 1;
			special_id[0] = '\0';
			while (parse_line[i] != ' ' && parse_line[i] != '\0') {
				special_id[i - 1] = parse_line[i];
				i++;
			}
			special_id[i - 1] = '\0';

			if (strcmp(special_id, "define") == 0) {
				char define_id[MAX_NAME_LENGTH];
				char define_value[MAX_NAME_LENGTH];

				int len = strlen(special_id) + 1;
				while (parse_line[len - 1] != ' ') {
					len++;
				}

				int i = 0;
				while (parse_line[len] != '\0' && parse_line[len] != ' ') {
					define_id[i] = parse_line[len];
					len++;
					i++;
				}
				define_id[i] = '\0';

				while (parse_line[len] == ' ') {
					len++;
				}

				int has_space = 0;
				int j = len;
				while (parse_line[j] != '\0' && parse_line[j] != '\n') {
					if (parse_line[j] == ' ') {
						has_space = 1;
						break;
					}
					j++;
				}

				if (has_space) {
					if (parse_line[len] == '"') {
						len++;
						int k = 0;
						while (parse_line[len] != '"' &&
							   parse_line[len] != '\0') {
							define_value[k] = parse_line[len];
							len++;
							k++;
						}
						define_value[k] = '\0';
						if (parse_line[len] != '"' || k == 0) {
							printf(
								"[ %sERROR%s ] %s has spaces in its value. It has to be enclosed in quotes (\")!\n",
								ANSI_COLOR_RED, ANSI_COLOR_RESET, define_id);
							exit(1);
						}
					} else {
						printf(
							"[ %sERROR%s ] %s has spaces in its value. It has to be enclosed in quotes (\")!\n",
							ANSI_COLOR_RED, ANSI_COLOR_RESET, define_id);
						exit(1);
					}
				} else {
					int k = 0;
					while (parse_line[len] != '\0' && parse_line[len] != ' ' &&
						   parse_line[len] != '\n') {
						define_value[k] = parse_line[len];
						len++;
						k++;
					}
					define_value[k] = '\0';
				}

				define_value[strcspn(define_value, "\n")] = '\0';

				add_macro(define_id, define_value);
				if (verbose)
					printf("Macro: %s=%s\n", define_id, define_value);
			}
			continue;
		}

		trim(line);
		if (line[0] == '\0' || line[0] == '#') {
			continue;
		}

		char *key = strtok(line, "=");
		char *value = strtok(NULL, "\n");

		if (value == NULL) {
			return 1;
		}

		char *k = key;
		while (isspace((unsigned char)*k))
			k++;
		char *end = k + strlen(k) - 1;
		while (end > k && isspace((unsigned char)*end))
			end--;
		*(end + 1) = '\0';
		key = k;

		if (strncmp(key, "CC_DEFINES", 10) == 0) {
			char *v = value;
			int len = strlen(v);
			int i, j;
			for (i = 0, j = 0; i < len; i++) {
				if (!isspace((unsigned char)v[i])) {
					v[j++] = v[i];
				}
			}
			v[j] = '\0';
		}

		int macro_use = 0;
		if (strchr(value, SPECIAL_PREFIX) != NULL) {
			macro_use = 1;
		}

		if (macro_use) {
			char macro_id[MAX_NAME_LENGTH];
			char *start = strchr(value, SPECIAL_PREFIX);
			while (start != NULL) {
				int i = 1;
				int inside_quotes = 0;
				char *p = start + 1;
				while (*p != '\0' && *p != SPECIAL_PREFIX) {
					if (*p == '"' && !inside_quotes) {
						break;
					}
					if (*p == '"') {
						inside_quotes = !inside_quotes;
					}
					macro_id[i - 1] = *p;
					i++;
					p++;
				}
				macro_id[i - 1] = '\0';

				if (macro_count == 0)
					exit(1);

				char *macro_value = get_macro_value(macro_id);

				if (macro_value == NULL) {
					printf("[ %sERROR%s ] Macro \"%s\" doesn't exist\n", "RED",
						   "RESET", macro_id);
					fclose(conf_file);
					exit(1);
				} else {
					macro_value[strcspn(macro_value, "\n")] = '\0';
					size_t len = strlen(macro_value);
					size_t replace_len = p - start;
					size_t remaining_len = strlen(p);

					memmove(start + len, p, remaining_len + 1);
					memcpy(start, macro_value, len);

					start = strchr(start + len, SPECIAL_PREFIX);
					free(macro_value);
				}
			}
		}

		if (strcmp(key, "CC_DEFINES") == 0 && strcmp(value, "[") == 0) {
			cc_defines = 1;
			cc_defines_started = 1;
			continue;
		}

		int quoted = 0;
		if (value[0] == '"' && value[strlen(value) - 1] == '"') {
			value[strlen(value) - 1] = '\0';
			value++;
			quoted = 1;
		}

		if (strcmp(key, "CC") == 0) {
			set_cc(2, (char *[]){ "cc", value });
		} else if (strcmp(key, "LD") == 0) {
			set_ld(2, (char *[]){ "ld", value });
		} else if (strcmp(key, "AS") == 0) {
			set_as(2, (char *[]){ "as", value });
		} else if (strcmp(key, "CC_FLAGS") == 0) {
			set_cc_flags(2, (char *[]){ "cc_flags", value });
		} else if (strcmp(key, "LD_FLAGS") == 0) {
			set_ld_flags(2, (char *[]){ "ld_flags", value });
		} else if (strcmp(key, "AS_FLAGS") == 0) {
			set_as_flags(2, (char *[]){ "as_flags", value });
		} else if (strcmp(key, "CORES") == 0 && value != NULL) {
			if (CORES != NULL) {
				free(CORES);
			}

			CORES = (char *)malloc(strlen(value) + 1);
			if (CORES != NULL) {
				strcpy(CORES, value);
			} else {
				fprintf(stderr, "Memory allocation failed for CORES\n");
				exit(EXIT_FAILURE);
			}
		} else if (strcmp(key, "MEMORY") == 0 && value != NULL) {
			if (MEMORY != NULL) {
				free(MEMORY);
			}

			MEMORY = (char *)malloc(strlen(value) + 1);
			if (MEMORY != NULL) {
				strcpy(MEMORY, value);
			} else {
				fprintf(stderr, "Memory allocation failed for MEMORY\n");
				exit(EXIT_FAILURE);
			}
		} else if (strcmp(key, "VERBOSE") == 0 && strcmp(value, "yes") == 0) {
			verbose = 1;
		} else if (strcmp(key, "VERBOSE") == 0 && strcmp(value, "no") == 0) {
			verbose = 0;
		} else if (strcmp(key, "VERBOSE") == 0 &&
				   (strcmp(value, "no") != 0 && strcmp(value, "yes") != 0)) {
			printf(
				"[ %sERROR%s ] Invalid value in config file! %s=%s. %s can only be \"yes\" or \"no\"!\n",
				ANSI_COLOR_RED, ANSI_COLOR_RESET, key, value, key);

			fclose(conf_file);
			exit(1);
		} else if (cc_defines) {
			char *name = key;
			char *val = value;

			if (name != NULL && val != NULL) {
				defines[define_count].name = (char *)malloc(strlen(name) + 1);
				defines[define_count].value = (char *)malloc(strlen(val) + 1);
				strcpy(defines[define_count].name, name);
				strcpy(defines[define_count].value, val);
				define_count++;

				size_t escaped_val_len = strlen(val) * 2 + 1;
				char *escaped_val = malloc(escaped_val_len);
				if (escaped_val == NULL) {
					printf("[ERROR] Memory allocation error.\n");
					return 1;
				}
				char *p = escaped_val;
				for (char *v = val; *v != '\0'; v++) {
					if (*v == '"') {
						*p++ = '\\';
					}
					*p++ = *v;
				}
				*p = '\0';

				size_t new_CC_FLAGS_len = (CC_FLAGS ? strlen(CC_FLAGS) : 0) +
										  strlen("-D") + strlen(name) +
										  strlen(escaped_val) + 6;
				if (CC_FLAGS != NULL) {
					new_CC_FLAGS_len += strlen(CC_FLAGS);
				}
				char *new_CC_FLAGS = (char *)malloc(new_CC_FLAGS_len);
				if (new_CC_FLAGS == NULL) {
					printf("[ERROR] Memory allocation error.\n");
					return 1;
				}
				strcpy(new_CC_FLAGS, CC_FLAGS ? CC_FLAGS : "");
				strcat(new_CC_FLAGS, " -D");
				strcat(new_CC_FLAGS, name);
				strcat(new_CC_FLAGS, "=");

				if (quoted) {
					strcat(new_CC_FLAGS, "\\\"\\\\\\\"");
					strcat(new_CC_FLAGS, escaped_val);
					strcat(new_CC_FLAGS, "\\\\\\\"\\\"");
				} else {
					strcat(new_CC_FLAGS, "\"");
					strcat(new_CC_FLAGS, escaped_val);
					strcat(new_CC_FLAGS, "\"");
				}

				free(escaped_val);

				if (CC_FLAGS != NULL) {
					free(CC_FLAGS);
				}

				CC_FLAGS = new_CC_FLAGS;
				if (verbose)
					printf("CC_FLAGS set to: %s\n", CC_FLAGS);
			}
		} else {
			printf("[ %sERROR%s ] Invalid value in config file! %s=%s!\n",
				   ANSI_COLOR_RED, ANSI_COLOR_RESET, key, value);

			fclose(conf_file);
			exit(1);
		}
	}

	fclose(conf_file);
	return 0;
}

int help_handler(int argc, char **argv)
{
	(void)argc;
	(void)argv;

	printf("Available commands:\n");
	printf("    %shelp%s\t\t\tDisplays this message.\n", ANSI_COLOR_CYAN,
		   ANSI_COLOR_RESET);
	printf("    %sclear%s\t\t\tClears the screen.\n", ANSI_COLOR_CYAN,
		   ANSI_COLOR_RESET);
	printf("    %sexit%s\t\t\tSimply exits the CLI.\n", ANSI_COLOR_CYAN,
		   ANSI_COLOR_RESET);
	printf("    %sverbose%s\t\t\tSets CLI to build with all logs.\n",
		   ANSI_COLOR_CYAN, ANSI_COLOR_RESET);
	printf("    %sconf%s\t\t\tDumps all config values.\n", ANSI_COLOR_CYAN,
		   ANSI_COLOR_RESET);
	printf("    %sload [file]%s\t\t\tLoads configuration from a file.\n",
		   ANSI_COLOR_CYAN, ANSI_COLOR_RESET);
	printf("    %sbuild%s\t\t\tBuild Leaf based on config (local).\n",
		   ANSI_COLOR_CYAN, ANSI_COLOR_RESET);
	printf("    %sclean%s\t\t\tCleans Leaf build files.\n", ANSI_COLOR_CYAN,
		   ANSI_COLOR_RESET);
	printf(
		"    %srun [args]%s\t\t\tRuns Leaf using qemu-system-x86_64 and user defined args.\n",
		ANSI_COLOR_CYAN, ANSI_COLOR_RESET);
	printf(
		"    %srun_uefi [args]%s\t\tRuns Leaf using qemu-system-x86_64 with UEFI firmware and user defined args.\n",
		ANSI_COLOR_CYAN, ANSI_COLOR_RESET);
	printf("    %sdump_macro%s\t\t\tDumps the macros defined.\n",
		   ANSI_COLOR_CYAN, ANSI_COLOR_RESET);

	return 0;
}

int exit_handler(int argc, char **argv)
{
	(void)argc;
	(void)argv;

	return 0x69;
}

int dump_conf(int argc, char **argv)
{
	printf("CC: %s\n", CC);
	printf("LD: %s\n", LD);
	printf("AS: %s\n", AS);
	printf("CC_FLAGS: %s\n", CC_FLAGS ? CC_FLAGS : "");
	printf("LD_FLAGS: %s\n", LD_FLAGS ? LD_FLAGS : "");
	printf("AS_FLAGS: %s\n", AS_FLAGS ? AS_FLAGS : "");

	for (int i = 0; i < MAX_DEFINES; i++) {
		if (defines[i].name != NULL && defines[i].value != NULL) {
			printf("DEFINE: %s=%s\n", defines[i].name, defines[i].value);
		}
	}

	for (int i = 0; i < MAX_MACROS; i++) {
		if (macros[i].name != NULL && macros[i].value != NULL) {
			printf("MACRO: %s=%s\n", macros[i].name, macros[i].value);
		}
	}

	printf("CORES: %s\n", CORES ? CORES : "");
	printf("MEMORY: %s\n", MEMORY ? MEMORY : "");
	return 0;
}

volatile int keep_spinning = 1;
pthread_t background_thread;

void *spinner_function(void *arg)
{
	const char spinner[] = "|/-\\";
	int i = 0;

	while (keep_spinning == 1) {
		if (!verbose) {
			printf("[ %s%c%s ] Building Leaf (CC: %s, LD: %s, AS: %s)\n",
				   ANSI_COLOR_CYAN, spinner[i], ANSI_COLOR_RESET, CC, LD, AS);
			fflush(stdout);
			usleep(100000);
			i = (i + 1) % 4;
			printf("\033[1A");
		}
	}

	return NULL;
}

void *_build(void *arg)
{
	char cmd[MAX_COMMAND_LENGTH];
	const char *cc = (CC != NULL) ? CC : "x86_64-elf-gcc";
	const char *ld = (LD != NULL) ? LD : "x86_64-elf-ld";
	const char *as = (AS != NULL) ? AS : "x86_64-elf-as";
	const char *cc_flags = (CC_FLAGS != NULL) ? CC_FLAGS : "";
	const char *ld_flags = (LD_FLAGS != NULL) ? LD_FLAGS : "";
	const char *as_flags = (AS_FLAGS != NULL) ? AS_FLAGS : "";
	const char *verbose_flag = verbose ? "--no-silent" : "--silent";

	snprintf(
		cmd, MAX_COMMAND_LENGTH,
		"make CC=\"%s\" LD=\"%s\" AS=\"%s\" CC_FLAGS=\"%s\" LD_FLAGS=\"%s\" AS_FLAGS=\"%s\" HOST_CC=\"%s\" HOST_LD=\"%s\" %s",
		cc, ld, as, cc_flags, ld_flags, as_flags, "cc", "ld", verbose_flag);

	if (verbose) {
		printf("%sBuild Command%s: %s\n", ANSI_COLOR_CYAN, ANSI_COLOR_RESET,
			   cmd);
	}

	FILE *fp;
	char buffer[1024];
	build_status = 0;

	fp = popen(cmd, "r");
	if (fp == NULL) {
		printf("[ %sERROR%s ] Failed to run make command.\n", ANSI_COLOR_RED,
			   ANSI_COLOR_RESET);
		return NULL;
	}

	while (fgets(buffer, sizeof(buffer), fp) != NULL) {
		if (build_status != 0 || verbose) {
			printf("%s", buffer);
		}
	}

	build_status = pclose(fp);

	if (WIFEXITED(build_status)) {
		int exit_status = WEXITSTATUS(build_status);
		if (exit_status != 0) {
			printf(
				"[ %sERROR%s ] Failed to build Leaf with make command. Exit status: %d",
				ANSI_COLOR_RED, ANSI_COLOR_RESET, exit_status);
			keep_spinning = 0;
		} else {
			keep_spinning = -1;
		}
	} else {
		printf("[ %sERROR%s ] make command did not exit normally.\n",
			   ANSI_COLOR_RED, ANSI_COLOR_RESET);
		keep_spinning = 0;
	}

	return NULL;
}

int build_leaf(int argc, char **argv)
{
	keep_spinning = 1;
	if (pthread_create(&background_thread, NULL, _build, NULL)) {
		fprintf(stderr, "Error creating thread\n");
		return 1;
	}

	spinner_function(NULL);

	pthread_join(background_thread, NULL);

	printf("\n");
	return (keep_spinning == 0) ? 0x32 : 0x420;
}

int clean_handler(int argc, char **argv)
{
	(void)argc;
	(void)argv;

	char cmd[MAX_COMMAND_LENGTH];
	snprintf(cmd, MAX_COMMAND_LENGTH, "make clean");

	if (verbose) {
		printf("%sClean Command%s: %s\n", ANSI_COLOR_CYAN, ANSI_COLOR_RESET,
			   cmd);
	}

	FILE *fp;
	char buffer[1024];

	fp = popen(cmd, "r");
	if (fp == NULL) {
		perror("Error executing clean command");
		return 1;
	}

	while (fgets(buffer, sizeof(buffer), fp) != NULL) {
		if (verbose) {
			printf("%s", buffer);
		}
	}

	int status = pclose(fp);
	if (status != 0) {
		printf("[ %sERROR%s ] Leaf clean failed.\n", ANSI_COLOR_RED,
			   ANSI_COLOR_RESET);
	} else {
		printf("[ %sSUCCESS%s ] Leaf cleaned successfully.\n", ANSI_COLOR_GREEN,
			   ANSI_COLOR_RESET);
	}

	return status;
}

int run_leaf(int argc, char **argv)
{
	if (MEMORY == NULL) {
		MEMORY = "2G";
	}
	if (CORES == NULL) {
		CORES = "2";
	}

	char full_command[MAX_COMMAND_LENGTH];
	snprintf(
		full_command, sizeof(full_command),
		"qemu-system-x86_64 -name \"Leaf OS\" -M q35 -boot d -serial stdio -serial file:boot.log "
		"-device piix3-ide,id=ide "
		"-drive file=Leaf.iso,format=raw,if=none,id=cdrom "
		"-device ide-cd,bus=ide.1,drive=cdrom "
		"-m %s -smp %s",
		MEMORY, CORES);

	for (int i = 1; i < argc; ++i) {
		strncat(full_command, " ",
				sizeof(full_command) - strlen(full_command) - 1);
		strncat(full_command, argv[i],
				sizeof(full_command) - strlen(full_command) - 1);
	}

	if (verbose) {
		printf("%sRun Command%s: %s\n", ANSI_COLOR_CYAN, ANSI_COLOR_RESET,
			   full_command);
	}

	printf("[ %sINFO%s ] COM1 -> stdout\n", ANSI_COLOR_CYAN, ANSI_COLOR_RESET);
	printf("[ %sINFO%s ] COM2 -> boot.log\n", ANSI_COLOR_CYAN,
		   ANSI_COLOR_RESET);
	printf("[ %sINFO%s ] Memory: %s\n", ANSI_COLOR_CYAN, ANSI_COLOR_RESET,
		   MEMORY);
	printf("[ %sINFO%s ] Cores: %s\n", ANSI_COLOR_CYAN, ANSI_COLOR_RESET,
		   CORES);
	printf("-------------------------------\n\n");

	int s = system(full_command);
	if (s != 0) {
		printf("[ %sERROR%s ] Running Leaf failed.\n", ANSI_COLOR_RED,
			   ANSI_COLOR_RESET);
		return 1;
	}

	return 0;
}

int run_uefi_leaf(int argc, char **argv)
{
	if (MEMORY == NULL) {
		MEMORY = "2G";
	}
	if (CORES == NULL) {
		CORES = "2";
	}

	char full_command[MAX_COMMAND_LENGTH];
	snprintf(
		full_command, sizeof(full_command),
		"qemu-system-x86_64 -name \"Leaf OS\" -M q35 -boot d -serial stdio -serial file:boot.log "
		"-device piix3-ide,id=ide "
		"-drive if=pflash,format=raw,readonly=on,file=bios/ovmf.fd "
		"-drive file=Leaf.iso,format=raw,if=none,id=cdrom "
		"-device ide-cd,bus=ide.1,drive=cdrom "
		"-m %s -smp %s",
		MEMORY, CORES);

	for (int i = 1; i < argc; ++i) {
		strncat(full_command, " ",
				sizeof(full_command) - strlen(full_command) - 1);
		strncat(full_command, argv[i],
				sizeof(full_command) - strlen(full_command) - 1);
	}

	if (verbose) {
		printf("%sRun Command%s: %s\n", ANSI_COLOR_CYAN, ANSI_COLOR_RESET,
			   full_command);
	}

	printf("[ %sINFO%s ] COM1 -> stdout\n", ANSI_COLOR_CYAN, ANSI_COLOR_RESET);
	printf("[ %sINFO%s ] COM2 -> boot.log\n", ANSI_COLOR_CYAN,
		   ANSI_COLOR_RESET);
	printf("[ %sINFO%s ] Memory: %s\n", ANSI_COLOR_CYAN, ANSI_COLOR_RESET,
		   MEMORY);
	printf("[ %sINFO%s ] Cores: %s\n", ANSI_COLOR_CYAN, ANSI_COLOR_RESET,
		   CORES);
	printf("-------------------------------\n\n");

	int s = system(full_command);
	if (s != 0) {
		printf("[ %sERROR%s ] Running Leaf failed.\n", ANSI_COLOR_RED,
			   ANSI_COLOR_RESET);
		return 1;
	}

	return 0;
}

int dump_macros(int argc, char **argv)
{
	for (int i = 0; i < macro_count; i++)
		printf("%s: %s\n", macros[i].name, macros[i].value);

	return 0;
}

Command commands[] = {
	{ "help", help_handler },	   { "exit", exit_handler },
	{ "clear", clear_screen },	   { "conf", dump_conf },
	{ "load", load_conf },		   { "build", build_leaf },
	{ "clean", clean_handler },	   { "run", run_leaf },
	{ "run_uefi", run_uefi_leaf }, { "verbose", set_verbose },
	{ "dump_macro", dump_macros }, { NULL, NULL },
};

int execute_command(char *command_line)
{
	char *arguments[MAX_ARGUMENTS];
	int argument_count = 0;
	char *token = strtok(command_line, " \t\n");
	while (token != NULL && argument_count < MAX_ARGUMENTS - 1) {
		arguments[argument_count++] = token;
		token = strtok(NULL, " \t\n");
	}
	arguments[argument_count] = NULL;

	if (argument_count == 0) {
		return 0;
	}

	for (int i = 0; commands[i].command != NULL; i++) {
		if (strcmp(arguments[0], commands[i].command) == 0) {
			return commands[i].handler(argument_count, arguments);
		}
	}

	printf(
		"[ %sERROR%s ] Command \"%s\" not found. Type \"help\" for a list of available commands.\n",
		ANSI_COLOR_RED, ANSI_COLOR_RESET, arguments[0]);
	return 1;
}

volatile sig_atomic_t config_changed = 0;

void *config_monitor(void *arg)
{
	time_t last_modified = 0;
	struct stat file_stat;
	const char *filename = (const char *)arg;

	while (1) {
		if (stat(filename, &file_stat) == 0) {
			if (file_stat.st_mtime > last_modified) {
				last_modified = file_stat.st_mtime;
				config_changed = 1;

				if (config_changed) {
					if (first_time) {
						first_time = 0;
						fflush(stdout);
						continue;
					}
					printf("[ %sINFO%s ] Config file changed! (Reloading)\n",
						   ANSI_COLOR_CYAN, ANSI_COLOR_RESET);
					load_conf(2, (char *[]){ "load", CONFIG_FILE });
					config_changed = 0;

					printf("%s> %s", ANSI_COLOR_BRIGHT_BLACK, ANSI_COLOR_RESET);
					fflush(stdout);
				}
			}
		} else {
			printf("[ %sERROR%s ] Failed to get file status.\n", ANSI_COLOR_RED,
				   ANSI_COLOR_RESET);
		}
		sleep(POLLING_INTERVAL);
	}
	return NULL;
}

int main(int argc, char *argv[])
{
	printf("%sLeaf Build Tool v%d.%d.%d (Use %shelp%s to get started)%s\n",
		   ANSI_COLOR_BRIGHT_GREEN, VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH,
		   ANSI_COLOR_CYAN, ANSI_COLOR_BRIGHT_GREEN, ANSI_COLOR_RESET);
	load_conf(2, (char *[]){ "load", CONFIG_FILE });

	pthread_t monitor_thread;
	if (pthread_create(&monitor_thread, NULL, config_monitor,
					   (void *)CONFIG_FILE) != 0) {
		fprintf(stderr, "Error creating config monitor thread.\n");
		return 1;
	}

	for (int i = 1; i < argc; i++) {
		if (execute_command(argv[i]) != 0) {
			return 1;
		}
	}

	char command_line[MAX_COMMAND_LENGTH];
	int status;

	while (1) {
		printf("%s> %s", ANSI_COLOR_BRIGHT_BLACK, ANSI_COLOR_RESET);
		fflush(stdout);
		if (fgets(command_line, MAX_COMMAND_LENGTH, stdin) == NULL) {
			break;
		}

		status = execute_command(command_line);

		if (status != 0) {
			if (status == 0x69) {
				free(CC);
				free(LD);
				free(AS);
				free(CC_FLAGS);
				free(LD_FLAGS);
				free(AS_FLAGS);
				return 1;
			} else if (status == 0x420) {
				printf("[ %sDONE%s ] Finished building Leaf!\n",
					   ANSI_COLOR_BRIGHT_GREEN, ANSI_COLOR_RESET);
			} else if (status == 0x32) {
				printf("[ %sERROR%s ] Failed to build Leaf!\n", ANSI_COLOR_RED,
					   ANSI_COLOR_RESET);
				continue;
			} else {
				printf("[ %sERROR%s ] Unknown error: %d.\n", ANSI_COLOR_RED,
					   ANSI_COLOR_RESET, status);
			}
		}
	}

	return 0;
}
