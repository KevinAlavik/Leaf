#ifndef COMMAND_H
#define COMMAND_H

typedef int (*CommandHandler)(int argc, char **argv);

typedef struct {
	const char *command;
	CommandHandler handler;
} Command;

extern Command all_commands[];
extern int num_commands;

int register_command(const char *command, CommandHandler handler);
int deregister_command(const char *command);
int execute_command(char *command_line);

#endif /* COMMAND_H */
