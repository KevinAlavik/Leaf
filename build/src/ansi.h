#ifndef ANSI_H
#define ANSI_H

// ANSI color codes
#define ANSI_COLOR_BLACK "\x1b[30m"
#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_WHITE "\x1b[37m"
#define ANSI_COLOR_RESET "\x1b[0m"

// Bright color codes
#define ANSI_COLOR_BRIGHT_BLACK "\x1b[30;1m"
#define ANSI_COLOR_BRIGHT_RED "\x1b[31;1m"
#define ANSI_COLOR_BRIGHT_GREEN "\x1b[32;1m"
#define ANSI_COLOR_BRIGHT_YELLOW "\x1b[33;1m"
#define ANSI_COLOR_BRIGHT_BLUE "\x1b[34;1m"
#define ANSI_COLOR_BRIGHT_MAGENTA "\x1b[35;1m"
#define ANSI_COLOR_BRIGHT_CYAN "\x1b[36;1m"
#define ANSI_COLOR_BRIGHT_WHITE "\x1b[37;1m"

// Background color codes
#define ANSI_COLOR_BG_BLACK "\x1b[40m"
#define ANSI_COLOR_BG_RED "\x1b[41m"
#define ANSI_COLOR_BG_GREEN "\x1b[42m"
#define ANSI_COLOR_BG_YELLOW "\x1b[43m"
#define ANSI_COLOR_BG_BLUE "\x1b[44m"
#define ANSI_COLOR_BG_MAGENTA "\x1b[45m"
#define ANSI_COLOR_BG_CYAN "\x1b[46m"
#define ANSI_COLOR_BG_WHITE "\x1b[47m"
#define ANSI_COLOR_BG_RESET "\x1b[0m"

// Bright background color codes
#define ANSI_COLOR_BG_BRIGHT_BLACK "\x1b[40;1m"
#define ANSI_COLOR_BG_BRIGHT_RED "\x1b[41;1m"
#define ANSI_COLOR_BG_BRIGHT_GREEN "\x1b[42;1m"
#define ANSI_COLOR_BG_BRIGHT_YELLOW "\x1b[43;1m"
#define ANSI_COLOR_BG_BRIGHT_BLUE "\x1b[44;1m"
#define ANSI_COLOR_BG_BRIGHT_MAGENTA "\x1b[45;1m"
#define ANSI_COLOR_BG_BRIGHT_CYAN "\x1b[46;1m"
#define ANSI_COLOR_BG_BRIGHT_WHITE "\x1b[47;1m"

// ANSI RGB color macro
#define ANSI_RGB(r, g, b) "\x1b[38;2;" #r ";" #g ";" #b "m"

#endif /* ANSI_H */
