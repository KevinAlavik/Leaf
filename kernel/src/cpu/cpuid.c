// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#include <cpu/cpuid.h>
#include <lib/std/types.h>

int CpuidString(int code, u32 where[4])
{
	asm volatile("cpuid"
				 : "=a"(*where), "=b"(*(where + 1)), "=c"(*(where + 2)),
				   "=d"(*(where + 3))
				 : "a"(code));
	return (int)where[0];
}

bool CpuidCheckFeature(u32 feat)
{
	unsigned int eax, unused, edx;
	__get_cpuid(1, &eax, &unused, &unused, &edx);
	return edx & feat;
}