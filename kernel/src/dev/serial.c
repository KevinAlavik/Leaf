// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#define SERIAL_IMPLEMENTATION
#include <dev/serial.h>

int SerialPortInitialize(u16 port)
{
	SerialIOWriteByte(port + 1, 0x00);
	SerialIOWriteByte(port + 3, 0x80);
	SerialIOWriteByte(port + 0, 0x03);
	SerialIOWriteByte(port + 1, 0x00);
	SerialIOWriteByte(port + 3, 0x03);
	SerialIOWriteByte(port + 2, 0xC7);
	SerialIOWriteByte(port + 4, 0x0B);
	SerialIOWriteByte(port + 4, 0x1E);
	SerialIOWriteByte(port + 0, 0xAE);

	if (SerialIOReadByte(port + 0) != 0xAE) {
		return 1;
	}

	SerialIOWriteByte(port + 4, 0x0F);
	return 0;
}

int SerialPortReceived(u16 port)
{
	return SerialIOReadByte(port + 5) & 1;
}

int SerialPortIsTransmitEmpty(u16 port)
{
	return SerialIOReadByte(port + 5) & 0x20;
}

u8 SerialPortRead(u16 port)
{
	while (SerialPortReceived(port) == 0)
		;

	return SerialIOReadByte(port);
}

void SerialPortWrite(u16 port, u8 data)
{
	while (SerialPortIsTransmitEmpty(port) == 0)
		;

	SerialIOWriteByte(port, data);
}

void SerialPortWriteString(u16 port, const char *data)
{
	while (*data != '\0') {
		SerialPortWrite(port, *data++);
	}
}
