// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#include <core/apic/ioapic.h>
#include <core/mm/vmm.h>
#include <kernel.h>

#undef ALIGN_UP
#undef ALIGN_DOWN

#define ALIGN_UP(x, base) (((x) + (base)-1) & ~((base)-1))
#define ALIGN_DOWN(x, base) ((x) & ~((base)-1))

void IOApicWrite(apicIOApic *ioapic, u32 reg, u32 val)
{
	*(volatile u32 *)((uptr)ioapic->ioapicAddress + hhdmOffset) = reg;
	*(volatile u32 *)((uptr)ioapic->ioapicAddress + hhdmOffset + 0x10) = val;
}

u32 IOApicRead(apicIOApic *ioapic, u32 reg)
{
	*(volatile u32 *)((uptr)ioapic->ioapicAddress + hhdmOffset) = reg;
	return *(volatile u32 *)((uptr)ioapic->ioapicAddress + hhdmOffset + 0x10);
}

void IOApicSetIRQ(u32 irq, u32 vector, u32 lapicId, bool unset)
{
	u16 isoFlags = 0;

	KernelLog("g_apicIsosCount: %d\n", g_apicIsosCount); // Debug logging

	for (int i = 0; i < g_apicIsosCount; i++) {
		KernelLog("g_apicIsos[%d]: %p\n", i, g_apicIsos[i]); // Debug logging

		if (g_apicIsos[i] == NULL) {
			KernelError("g_apicIsos[%d] is NULL\n", i); // Error logging
			continue;
		}

		KernelLog("g_apicIsos[%d]->irq: %d\n", i,
				  g_apicIsos[i]->irq); // Debug logging

		if (g_apicIsos[i]->irq == irq) {
			irq = g_apicIsos[i]->gsi;
			isoFlags = g_apicIsos[i]->flags;
			break;
		}
	}

	apicIOApic *ioapic = NULL;

	for (usize i = 0; i < g_acpiIOApicCount; i++) {
		KernelLog("Checking IOAPIC %d, gsiBase: %d\n", i,
				  g_acpiIOApic[i]->gsiBase); // Debug logging

		if (g_acpiIOApic[i]->gsiBase <= irq &&
			irq < g_acpiIOApic[i]->gsiBase +
					  ((IOApicRead(g_acpiIOApic[i], 0x1) & 0xFF0000) >> 16)) {
			ioapic = g_acpiIOApic[i];
		}
	}

	if (ioapic == NULL) {
		KernelError("IRQ %u isn't mapped to any IOAPIC\n", (u64)irq);
		return;
	}

	u32 entryHigh = lapicId << 24;
	u32 entryLow = vector;

	if (isoFlags & 0x3)
		entryLow |= 1u << 13;
	if (isoFlags & 0xC)
		entryLow |= 1u << 14;

	if (unset) {
		entryHigh = 0;
		entryLow = 0;
	}

	u32 idx = (irq - ioapic->gsiBase) * 2;
	IOApicWrite(ioapic, 0x10 + idx + 1, entryHigh);
	IOApicWrite(ioapic, 0x10 + idx, entryLow);
}

void IOApicRedirectIRQ(u32 irq, u32 vector, u32 id)
{
	IOApicSetIRQ(irq, vector, id, 0);
}

void IOApicRemoveIRQ(u32 irq, u32 id)
{
	IOApicSetIRQ(irq, 0, id, 1);
}

int IOApicInitialize()
{
	// Mask all interrupts.
	SerialIOWriteByte(0x21, 0xFF);
	SerialIOWriteByte(0xA1, 0xFF);

	// Map the IOAPIC.
	for (int i = 0; i < g_acpiIOApicCount; i++) {
		VmmMap(
			&kernelPagemap,
			(u64)ALIGN_DOWN((uptr)g_acpiIOApic[i]->ioapicAddress + hhdmOffset,
							PAGE_SIZE),
			(u64)ALIGN_DOWN((uptr)g_acpiIOApic[i]->ioapicAddress, PAGE_SIZE),
			VMM_FLAG_PRESENT | VMM_FLAG_WRITE);
	}

	return 0;
}