// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#define GDT_IMPLEMENTATION
#include <core/gdt.h>

gdtPointer_t gdtPointer;
gdtEntry_t gdtEntries[4];

int GdtInitialize()
{
	gdtEntries[0] = (gdtEntry_t){ 0, 0, 0, 0, 0 };
	gdtEntries[1] = (gdtEntry_t){ 0, 0, 0, 0b10011010, 0b10100000, 0 };
	gdtEntries[2] = (gdtEntry_t){ 0, 0, 0, 0b10010010, 0b10100000, 0 };
	gdtEntries[3] = (gdtEntry_t){ 0, 0, 0, 0b11111010, 0b10100000, 0 };
	gdtEntries[4] = (gdtEntry_t){ 0, 0, 0, 0b11110010, 0b10100000, 0 };

	gdtPointer.limit = (u16)(sizeof(gdtEntries) - 1);
	gdtPointer.base = (uptr)&gdtEntries;

	GdtFlush(gdtPointer);
	return 0;
}
