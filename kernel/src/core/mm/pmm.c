// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#include <core/mm/pmm.h>
#include <kernel.h>
#include <lib/posix/string.h>
#include <lib/posix/assert.h>

u8 *bitmap;
u64 bitmapPages;
u64 bitmapSize;

// TODO: Move all these functions into its own files.
void BitmapSet(u8 *bitmap, u64 bit)
{
	bitmap[bit / 8] |= 1 << (bit % 8);
}

void BitmapClear(u8 *bitmap, u64 bit)
{
	bitmap[bit / 8] &= ~(1 << (bit % 8));
}

u8 BitmapGet(u8 *bitmap, u64 bit)
{
	return bitmap[bit / 8] & (1 << (bit % 8));
}

// PMM Entry point.
int PmmInitialize()
{
	u64 topAddress;
	u64 higherAddress = 0;

	for (u64 entryCount = 0; entryCount < memmapResponse->entry_count;
		 entryCount++) {
		struct limine_memmap_entry *entry = memmapResponse->entries[entryCount];

		if (entry->type == LIMINE_MEMMAP_USABLE) {
			topAddress = entry->base + entry->length;
			if (topAddress > higherAddress)
				higherAddress = topAddress;
		}
	}
	bitmapPages = higherAddress / PAGE_SIZE;
	bitmapSize = ALIGN_UP(bitmapPages / 8, PAGE_SIZE);

	for (u64 entryCount = 0; entryCount < memmapResponse->entry_count;
		 entryCount++) {
		struct limine_memmap_entry *entry = memmapResponse->entries[entryCount];

		if (entry->type == LIMINE_MEMMAP_USABLE) {
			if (entry->length >= bitmapSize) {
				bitmap = (u8 *)(entry->base + hhdmOffset);
				memset(bitmap, 0xFF, bitmapSize);
				entry->base += bitmapSize;
				entry->length -= bitmapSize;
				break;
			}
		}
	}

	for (u64 entryCount = 0; entryCount < memmapResponse->entry_count;
		 entryCount++) {
		struct limine_memmap_entry *entry = memmapResponse->entries[entryCount];

		if (entry->type == LIMINE_MEMMAP_USABLE) {
			for (u64 i = 0; i < entry->length; i += PAGE_SIZE) {
				BitmapClear(bitmap, (entry->base + i) / PAGE_SIZE);
			}
		}
	}

	return 0;
}

void *PmmRequestPages(usize numPages)
{
	u64 last_allocated_index = 0;

	while (1) {
		if (!BitmapGet(bitmap, last_allocated_index)) {
			usize consecutive_free_pages = 1;

			for (usize i = 1; i < numPages; ++i) {
				if (!BitmapGet(bitmap, last_allocated_index + i)) {
					++consecutive_free_pages;
				} else {
					consecutive_free_pages = 0;
					break;
				}
			}

			if (consecutive_free_pages == numPages) {
				for (usize i = 0; i < numPages; ++i) {
					BitmapSet(bitmap, last_allocated_index + i);
				}

				return (void *)(last_allocated_index * PAGE_SIZE);
			}
		}

		++last_allocated_index;

		if (last_allocated_index >= bitmapPages) {
			return NULL;
		}
	}
}

void PmmFreePages(void *ptr, usize numPages)
{
	assert(ptr != NULL);
	assert((u64)ptr % PAGE_SIZE == 0);
	assert(numPages > 0);

	u64 startBitIdx = ((u64)ptr / PAGE_SIZE);

	for (usize i = startBitIdx; i < numPages; ++i) {
		BitmapClear(bitmap, i);
	}
}

u64 PmmGetTotal()
{
	u64 totalMemory = 0;
	for (u64 entryCount = 0; entryCount < memmapResponse->entry_count;
		 entryCount++) {
		struct limine_memmap_entry *entry = memmapResponse->entries[entryCount];
		if (entry->type == LIMINE_MEMMAP_USABLE) {
			totalMemory += entry->length;
		}
	}

	return totalMemory;
}

u64 PmmGetFree()
{
	u64 freeMemory = 0;
	for (u64 i = 0; i < bitmapPages; i++) {
		if (!BitmapGet(bitmap, i)) {
			freeMemory += PAGE_SIZE;
		}
	}

	return freeMemory;
}

void *PmmAlloc(usize size)
{
	char *pointer =
		(char *)PHYS_TO_VIRT(PmmRequestPages((size / PAGE_SIZE) + 1));
	if (pointer == NULL) {
		return NULL;
	}
	*((usize *)pointer) = size;
	pointer += sizeof(usize);
	return (void *)pointer;
}

void *PmmRealloc(void *old, usize size)
{
	if (old == NULL) {
		return PmmAlloc(size);
	}

	usize old_size = *((usize *)old - 1);

	usize new_num_pages = (size / PAGE_SIZE) + 1;
	usize old_num_pages = (old_size / PAGE_SIZE) + 1;

	if (new_num_pages > old_num_pages) {
		void *new_mem =
			PHYS_TO_VIRT(PmmRequestPages(new_num_pages - old_num_pages));
		if (new_mem == NULL) {
			return NULL;
		}

		memcpy(new_mem, old, old_size);
		PmmFree(old);

		return new_mem;
	} else {
		return old;
	}
}

void PmmFree(void *pointer)
{
	if (pointer == NULL) {
		return;
	}
	pointer -= sizeof(usize);
	usize size = *((usize *)pointer);
	PmmFreePages(pointer, (size / PAGE_SIZE) + 1);
}
