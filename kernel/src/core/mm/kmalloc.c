// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#include <core/mm/kmalloc.h>
#include <lib/posix/string.h>

heapCtx *globalHeap;

int KMallocInitialize(heapCtx *kernelHeap)
{
	if (kernelHeap == NULL)
		return 1;
	else
		globalHeap = kernelHeap;

	return 0;
}

void *KMallocMalloc(usize size)
{
	return HeapAlloc(globalHeap, size);
}

void KMallocFree(void *ptr)
{
	HeapFree(globalHeap, ptr);
}

void *KMallocRealloc(void *ptr, usize size)
{
	if (ptr == NULL) {
		return PmmAlloc(size);
	}

	usize old_size = *((usize *)ptr - 1);

	if (size > old_size) {
		void *new_mem = HeapAlloc(globalHeap, size);
		if (new_mem == NULL) {
			return NULL;
		}

		memcpy(new_mem, ptr, old_size);
		PmmFree(ptr);

		return new_mem;
	} else {
		return ptr;
	}
}

void *KMallocCalloc(usize nmemb, usize size)
{
	void *mem = HeapAlloc(globalHeap, nmemb * size);
	if (mem == NULL) {
		return NULL;
	}

	memset(mem, 0, nmemb * size);
	return mem;
}
