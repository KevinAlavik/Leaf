// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#include <core/mm/heap.h>
#include <lib/posix/string.h>
#include <lib/std/io/stdout.h>
#include <kernel.h>

int HeapInitialize(heapCtx *heap, usize maxSize)
{
	if (heap == NULL) {
		return 1;
	}

	heap->maxSize = maxSize;
	heap->size = 0;
	heap->maxChunkCount = maxSize / HEAP_CHUNK_SIZE;
	heap->chunkCount = 1;

	if (heap->chunkCount > heap->maxChunkCount) {
		return 1;
	}

	heap->chunks = (heapChunk **)PmmAlloc(HEAP_CHUNK_SIZE);
	if (heap->chunks == NULL) {
		return 1;
	}

	// Allocate memory for the first chunk, 4096 bytes or 1 page
	heap->chunks[0] = (heapChunk *)PmmAlloc(HEAP_CHUNK_SIZE);
	if (heap->chunks[0] == NULL) {
		if (heap->chunks != NULL) {
			PmmFreePages(heap->chunks, 1);
			heap->chunks = NULL;
		}
		return 1;
	}

	// Initial chunk size is 0 since there is no value in the chunk.
	heap->chunks[0]->size = 0;
	heap->chunks[0]->capacity = HEAP_CHUNK_SIZE;

	return 0;
}

void *HeapAlloc(heapCtx *ctx, usize size)
{
	if (ctx == NULL)
		return NULL;

	if (size == 0) {
		return NULL;
	}

#if _LEAF_VERBOSE_HEAP
	KernelLog("Allocating %d byte(s)!\n", size);
#endif

	if (size > ctx->maxSize) {
		KernelLog(
			"Attempting to allocate more bytes than possible, avoiding overflow\n");
		return NULL;
	}

	for (int i = 0; i < ctx->chunkCount; i++) {
		heapChunk *chunk = &*ctx->chunks[i];

		if (size > chunk->capacity) {
			KernelWarn(
				"Attempting to allocate %d bytes which is more then the default size for a chunk. Creating a new chunk and resizing it!\n",
				size);

			if ((HEAP_CHUNK_SIZE * (ctx->chunkCount)) + size > ctx->maxSize) {
				KernelError(
					"Attempting to resize a chunk larger than possible. Maximum size of heap: %d byes, Size if we resize: %d bytes!\n",
					ctx->maxSize, (HEAP_CHUNK_SIZE * (ctx->chunkCount)));
				return NULL;
			} else {
				ctx->chunks = PmmRealloc(
					ctx->chunks, (HEAP_CHUNK_SIZE * (ctx->chunkCount)) + size);
				ctx->chunks[ctx->chunkCount] = PmmAlloc(size);
				if (ctx->chunks[ctx->chunkCount] == NULL) {
					if (ctx->chunks != NULL) {
						PmmFreePages(ctx->chunks, 1);
						ctx->chunks = NULL;
					}
					return NULL;
				}

				ctx->chunks[ctx->chunkCount]->size = 0;
				ctx->chunks[ctx->chunkCount]->capacity = size;
				ctx->chunkCount++;

				continue;
			}
		}

		if (chunk->capacity - chunk->size < size) {
#if _LEAF_VERBOSE_HEAP
			KernelLog(
				"Chunk %d dont have enough space! (%d needed out of %d existing space)\n",
				i, size, HEAP_CHUNK_SIZE - chunk->size);
#endif

			if (ctx->chunkCount != ctx->maxChunkCount) {
				ctx->chunks = PmmRealloc(
					ctx->chunks, HEAP_CHUNK_SIZE * (ctx->chunkCount + 1));
				ctx->chunks[ctx->chunkCount] =
					(heapChunk *)PmmAlloc(HEAP_CHUNK_SIZE);
				if (ctx->chunks[ctx->chunkCount] == NULL) {
					if (ctx->chunks != NULL) {
						PmmFreePages(ctx->chunks, 1);
						ctx->chunks = NULL;
					}
					return NULL;
				}

				ctx->chunks[ctx->chunkCount]->size = 0;
				ctx->chunks[ctx->chunkCount]->capacity = HEAP_CHUNK_SIZE;
				ctx->chunkCount++;
#if _LEAF_VERBOSE_HEAP
				KernelLog("Created new chunk %d\n", ctx->chunkCount - 1);
#endif
				continue;
			} else {
				// Avoid overflowing. TODO: Garbage collection
				KernelLog("Failed to create chunk: Max chunks reached!\n");
				continue;
			}
		}

#if _LEAF_VERBOSE_HEAP
		KernelLog(
			"Chunk %d has enough space! (%d bytes will be left in chunk)\n", i,
			(chunk->capacity - chunk->size) - size);
#endif

		if (chunk->data == NULL) {
#if _LEAF_VERBOSE_HEAP
			KernelLog("No data present in chunk %d\n", i);
#endif
			chunk->data = (void **)PmmAlloc(size);
			if (chunk->data == NULL)
				return NULL;

			chunk->data[0] = PmmAlloc(size);
			if (chunk->data[0] == NULL)
				return NULL;

			chunk->size += size;
#if _LEAF_VERBOSE_HEAP
			KernelLog("Allocated %d byte(s) on empty chunk %d, data idx: 0\n",
					  size, i);
#endif
			return chunk->data[0];
		} else {
			int idx = 0;
			while (chunk->data[idx] != NULL) {
				idx++;
			}
			chunk->data[idx] = PmmAlloc(size);
			if (chunk->data[idx] == NULL)
				return NULL;

			chunk->size += size;
#if _LEAF_VERBOSE_HEAP
			KernelLog("Allocated %d byte(s) on empty chunk %d, data idx: %d\n",
					  size, i, idx);
#endif
			return chunk->data[idx];
		}

		KernelLog("Failed to allocate %d byte(s)!\n", size);
		return NULL;
	}

	return NULL;
}

void HeapFree(heapCtx *ctx, void *ptr)
{
	if (ctx == NULL || ptr == NULL)
		return;

	for (int i = 0; i < ctx->chunkCount; i++) {
		heapChunk *chunk = ctx->chunks[i];

		if (chunk->data != NULL) {
			int idx = 0;
			while (chunk->data[idx] != NULL) {
				if ((u64)chunk->data[idx] == (u64)ptr) {
					void *pointer = &*(char *)chunk->data[idx];
					pointer -= sizeof(usize);
					usize size = *((usize *)pointer);

#if _LEAF_VERBOSE_HEAP
					KernelLog(
						"Found pointer at chunk[%d]->data[%d] size: %d bytes\n",
						i, idx, size);
#endif

					PmmFreePages(pointer, (size / PAGE_SIZE) + 1);
					chunk->data[idx] = NULL;
					chunk->size -= size;
#if _LEAF_VERBOSE_HEAP
					KernelLog("Freed %d byte(s) on chunk %d\n", size, i);
#endif

					return;
				}
				idx++;
			}

			// Didnt find pointer on this block
			continue;
		}
	}
}

void HeapTestVisualizeChunks(heapCtx *ctx)
{
	for (int i = 0; i < ctx->chunkCount; i++) {
		heapChunk *chunk = ctx->chunks[i];
		StdoutPrintf("Chunk [%d] (Size: %d bytes, Capacity: %d bytes): %p\n", i,
					 chunk->size, chunk->capacity, chunk);
		int idx = 0;
		while (chunk->data[idx] != NULL) {
			StdoutPrintf(" Data [%d]: %p\n", idx, chunk->data[idx]);
			idx++;
		}
	}
}

void HeapTestVisualizeHeap(heapCtx *ctx)
{
	for (int i = 0; i < ctx->chunkCount; i++) {
		heapChunk *chunk = ctx->chunks[i];
		StdoutPrintf("Chunk [%d] (Size: %d bytes, Capacity: %d bytes): %p\n", i,
					 chunk->size, chunk->capacity, chunk);
		int idx = 0;
		while (idx < chunk->size) {
			StdoutPrintf(" Data [%d]: %p\n", idx, chunk->data[idx]);
			idx++;
		}
		StdoutPrintf("\n");
	}
}
