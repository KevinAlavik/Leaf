// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#include <core/mm/vmm.h>
#include <kernel.h>

#include <lib/posix/string.h>

void *VmmGetPhysicalAddress(vmmCtx *ctx, u64 virtualAddress)
{
	const u16 offset = (u16)((virtualAddress & 0x000000000FFF) >> 0);
	const u16 PT_i = (u16)((virtualAddress & 0x0000001FF000) >> 12);
	const u16 PD_i = (u16)((virtualAddress & 0x00003FE00000) >> 21);
	const u16 PDP_i = (u16)((virtualAddress & 0x007FC0000000) >> 30);
	const u16 PML4_i = (u16)((virtualAddress & 0xFF8000000000) >> 39);

	PML4E PML4 = ctx->pml4Entries[PML4_i];
	if (PML4.present == 0)
		return NULL;

	PML3E PML3 =
		(((PML3E *)PHYS_TO_VIRT((void *)((u64)PML4.address << 12)))[PDP_i]);
	if (PML3.present == 0)
		return NULL;

	PML2E PML2 =
		(((PML2E *)PHYS_TO_VIRT((void *)((u64)PML3.address << 12)))[PD_i]);
	if (PML2.present == 0)
		return NULL;

	PML1E PML1 =
		(((PML1E *)PHYS_TO_VIRT((void *)((u64)PML2.address << 12)))[PT_i]);
	if (PML1.present == 0)
		return NULL;

	return (void *)(((u64)PML1.address << 12) | offset);
}

void VmmMap(vmmCtx *ctx, u64 virtualAddress, u64 physicalAddress, u32 flags)
{
	const u16 PT_i = (u16)((virtualAddress & 0x0000001FF000) >> 12);
	const u16 PD_i = (u16)((virtualAddress & 0x00003FE00000) >> 21);
	const u16 PDP_i = (u16)((virtualAddress & 0x007FC0000000) >> 30);
	const u16 PML4_i = (u16)((virtualAddress & 0xFF8000000000) >> 39);

	PML4E PML4 = ctx->pml4Entries[PML4_i];
	if (PML4.present == 0) {
		u64 temp = (u64)((flags & 0x0FFF) | ((u64)(flags & 0x07FF0000) << 36));
		PML4 = *(PML4E *)(&temp);
		PML4.present = 1;
		PML4.address = (u64)PmmRequestPages(1) >> 12;
		memset((void *)PHYS_TO_VIRT((u64)PML4.address << 12), 0, PAGE_SIZE);
		ctx->pml4Entries[PML4_i] = PML4;
	} else {
		u64 temp = *(u64 *)(&PML4);
		temp |= flags & 0xFFF;
		temp |= (u64)(flags & 0x7FF0000) << 36;
		ctx->pml4Entries[PML4_i] = *(PML4E *)&temp;
	}

	PML3E PML3 = ((PML3E *)PHYS_TO_VIRT((u64)PML4.address << 12))[PDP_i];
	if (PML3.present == 0) {
		u64 temp = (u64)((flags & 0x0FFF) | ((u64)(flags & 0x07FF0000) << 36));
		PML3 = *(PML3E *)(&temp);
		PML3.present = 1;
		PML3.address = (u64)PmmRequestPages(1) >> 12;
		memset((void *)PHYS_TO_VIRT((u64)PML3.address << 12), 0, PAGE_SIZE);
		((PML3E *)PHYS_TO_VIRT((u64)PML4.address << 12))[PDP_i] = PML3;
	} else {
		u64 temp = *(u64 *)(&PML3);
		temp |= flags & 0xFFF;
		temp |= (u64)(flags & 0x7FF0000) << 36;
		((PML3E *)PHYS_TO_VIRT((u64)PML4.address << 12))[PDP_i] =
			*(PML3E *)&temp;
	}

	PML2E PML2 = ((PML2E *)PHYS_TO_VIRT((u64)PML3.address << 12))[PD_i];
	if (PML2.present == 0) {
		u64 temp = (u64)((flags & 0x0FFF) | ((u64)(flags & 0x07FF0000) << 36));
		PML2 = *(PML2E *)(&temp);
		PML2.present = 1;
		PML2.address = (u64)PmmRequestPages(1) >> 12;
		memset((void *)PHYS_TO_VIRT((u64)PML2.address << 12), 0, PAGE_SIZE);
		((PML2E *)PHYS_TO_VIRT((u64)PML3.address << 12))[PD_i] = PML2;
	} else {
		u64 temp = *(u64 *)(&PML2);
		temp |= flags & 0xFFF;
		temp |= (u64)(flags & 0x7FF0000) << 36;
		((PML2E *)PHYS_TO_VIRT((u64)PML3.address << 12))[PD_i] =
			*(PML2E *)&temp;
	}

	u64 temp = (u64)((flags & 0x0FFF) | ((u64)(flags & 0x0FFF0000) << 36));
	PML1E PML1 = *(PML1E *)(&temp);
	PML1.address = (physicalAddress >> 12);
	((PML1E *)PHYS_TO_VIRT((u64)PML2.address << 12))[PT_i] = PML1;
}

void VmmMapRange(vmmCtx *ctx, u64 virtualAddressStart, u64 physicalAddress,
				 u64 virtualAddressEnd, u32 flags)
{
	for (; virtualAddressStart < virtualAddressEnd;
		 virtualAddressStart += PAGE_SIZE, physicalAddress += PAGE_SIZE) {
		VmmMap(ctx, virtualAddressStart, physicalAddress, flags);
	}
}

void VmmUnmap(vmmCtx *ctx, u64 virtualAddress)
{
	const u16 PT_i = (u16)((virtualAddress & 0x0000001FF000) >> 12);
	const u16 PD_i = (u16)((virtualAddress & 0x00003FE00000) >> 21);
	const u16 PDP_i = (u16)((virtualAddress & 0x007FC0000000) >> 30);
	const u16 PML4_i = (u16)((virtualAddress & 0xFF8000000000) >> 39);

	PML4E PML4 = ctx->pml4Entries[PML4_i];
	if (PML4.present == 0) {
		return;
	}

	PML3E *PML3_table = (PML3E *)PHYS_TO_VIRT((u64)PML4.address << 12);
	PML3E PML3 = PML3_table[PDP_i];
	if (PML3.present == 0) {
		return;
	}

	PML2E *PML2_table = (PML2E *)PHYS_TO_VIRT((u64)PML3.address << 12);
	PML2E PML2 = PML2_table[PD_i];
	if (PML2.present == 0) {
		return;
	}

	PML1E *PML1_table = (PML1E *)PHYS_TO_VIRT((u64)PML2.address << 12);
	PML1E PML1 = PML1_table[PT_i];
	if (PML1.present == 0) {
		return;
	}

	PML1_table[PT_i] = (PML1E){ 0 };
	__asm__ volatile("invlpg (%0)" ::"r"(virtualAddress) : "memory");
}

int _VmmMapKernel(vmmCtx *ctx)
{
	VmmMapRange(ctx, (uint64_t)&__kernel_start,
				(uint64_t)&__kernel_start -
					(uint64_t)kernelAddressResponse->virtual_base +
					(uint64_t)kernelAddressResponse->physical_base,
				(uint64_t)&__kernel_end,
				VMM_FLAG_PRESENT | VMM_FLAG_EXECUTE_DISABLE);

	VmmMapRange(ctx, (uint64_t)&__text_start,
				(uint64_t)&__text_start -
					(uint64_t)kernelAddressResponse->virtual_base +
					(uint64_t)kernelAddressResponse->physical_base,
				(uint64_t)&__text_end, VMM_FLAG_PRESENT);

	uint64_t aligned_text_end =
		ALIGN_ADDRESS_UP((uint64_t)&__text_end, PAGE_SIZE);
	VmmMapRange(ctx, (uint64_t)&__rodata_start,
				(uint64_t)&__rodata_start -
					(uint64_t)kernelAddressResponse->virtual_base +
					(uint64_t)kernelAddressResponse->physical_base,
				aligned_text_end, VMM_FLAG_PRESENT | VMM_FLAG_EXECUTE_DISABLE);

	uint64_t aligned_rodata_end =
		ALIGN_ADDRESS_UP((uint64_t)&__rodata_end, PAGE_SIZE);
	VmmMapRange(ctx, (uint64_t)&__data_start,
				(uint64_t)&__data_start -
					(uint64_t)kernelAddressResponse->virtual_base +
					(uint64_t)kernelAddressResponse->physical_base,
				aligned_rodata_end,
				VMM_FLAG_PRESENT | VMM_FLAG_WRITE | VMM_FLAG_EXECUTE_DISABLE);

	uint64_t aligned_data_end =
		ALIGN_ADDRESS_UP((uint64_t)&__data_end, PAGE_SIZE);
	VmmMapRange(ctx, (uint64_t)&__bss_start,
				(uint64_t)&__bss_start -
					(uint64_t)kernelAddressResponse->virtual_base +
					(uint64_t)kernelAddressResponse->physical_base,
				aligned_data_end,
				VMM_FLAG_PRESENT | VMM_FLAG_WRITE | VMM_FLAG_EXECUTE_DISABLE);

	for (uint64_t entryCount = 0; entryCount < memmapResponse->entry_count;
		 entryCount++) {
		struct limine_memmap_entry *entry = memmapResponse->entries[entryCount];

		if (entry == NULL) {
			return 1;
		}

		if ((entry->base + entry->length) < 0x100000000) {
			continue;
		}

		uint64_t length = (entry->length + 0xfff) & ~0xfff;

		for (uint64_t j = 0; j < length; j += 0x1000) {
			VmmMapRange(
				ctx, (uint64_t)entry->base + hhdmOffset,
				(uint64_t)entry->base + j, (uint64_t)entry->base + length,
				VMM_FLAG_PRESENT | VMM_FLAG_WRITE | VMM_FLAG_EXECUTE_DISABLE);
		}
	}

	return 0;
}

int VmmInitialize(vmmCtx *kernelCtx)
{
	memset(&kernelCtx->pml4Entries, 0, sizeof(&kernelCtx->pml4Entries));
	for (u64 addr = 0; addr < (4UL * 1024UL * 1024UL * 10); addr += PAGE_SIZE) {
		VmmMap(kernelCtx, (u64)PHYS_TO_VIRT(addr), addr,
			   VMM_FLAG_PRESENT | VMM_FLAG_WRITE | VMM_FLAG_EXECUTE_DISABLE);
	}

	return _VmmMapKernel(kernelCtx);
}
