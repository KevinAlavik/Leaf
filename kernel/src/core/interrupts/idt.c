// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#define IDT_IMPLEMENTATION
#include <core/interrupts/idt.h>
#define CPU_UTILS_IMPLEMENTATION
#include <cpu/utils.h>
#include <lib/std/io/stdout.h>
#include <lib/std/io/dbgout.h>
#include <dev/serial.h>
#include <core/apic/lapic.h>
#include <core/apic/ioapic.h>

idtEntry_t idtEntries[IDT_ENTRY_COUNT];
idtPointer_t idtPointer;
extern u64 isrTable[];
irqHandler_t irqHandlers[16];

static const char *exceptionStrings[32] = { "Division By Zero",
											"Debug",
											"Nonmaskable Interrupt",
											"Breakpoint",
											"Overflow",
											"Bound Range Exceeded",
											"Invalid Opcode",
											"Device Not Available",
											"Double Fault",
											"Coprocessor Segment Overrun",
											"Invalid TSS",
											"Segment Not Present",
											"Stack Segment Fault",
											"General Protection Fault",
											"Page Fault",
											"Reserved",
											"x87 FPU Error"
											"Alignment Check",
											"Machine Check",
											"Simd Exception",
											"Virtualization Exception",
											"Control Protection Exception",
											"Reserved",
											"Reserved",
											"Reserved",
											"Reserved",
											"Reserved",
											"Reserved",
											"Hypervisor Injection Exception",
											"VMM Communication Exception",
											"Security Exception",
											"Reserved" };

int IdtInitialize()
{
	idtPointer.limit = sizeof(idtEntry_t) * IDT_ENTRY_COUNT - 1;
	idtPointer.base = (uptr)&idtEntries;

	STI();
	for (int i = 0; i < 16; i++) {
		irqHandlers[i] = NULL;
	}

	for (int i = 0; i < IDT_ENTRY_COUNT; ++i) {
		IdtSetGate(idtEntries, i, isrTable[i], 0x08, 0x8E);
	}

	IdtLoad((u64)&idtPointer);
	CLI();
	return 0;
}

void IdtExcpHandler(intFrame_t frame)
{
	if (frame.vector < 0x20) {
		StdoutPrintf("\n\033[38;2;255;255;255m\033[48;2;255;0;0m");
		StdoutPrintf("#######################################\n");
		StdoutPrintf("###########  KERNEL PANIC  ############\n");
		StdoutPrintf("#######################################\033[0m\n\n");

		StdoutPrintf("\033[1mPanic at address: \033[0m0x%.16llx\n", frame.rip);
		StdoutPrintf("\033[1mException type:   \033[0m%d (%s)\n", frame.vector,
					 exceptionStrings[frame.vector]);
		StdoutPrintf("\033[1mError code:       \033[0m0x%.16llx\n\n",
					 frame.err);

		// Parse the error code for page faults
		if (frame.vector == 14) {
			unsigned long long p = frame.err & 1;
			unsigned long long wr = (frame.err >> 1) & 1;
			unsigned long long us = (frame.err >> 2) & 1;
			unsigned long long rsvd = (frame.err >> 3) & 1;
			unsigned long long id = (frame.err >> 4) & 1;

			StdoutPrintf("\033[1mPage Fault Error Code Details:\033[0m\n");
			StdoutPrintf("  \033[1mP:    \033[0m%s\n",
						 p ? "Protection violation" : "Non-present page");
			StdoutPrintf("  \033[1mWR:   \033[0m%s\n", wr ? "Write" : "Read");
			StdoutPrintf("  \033[1mUS:   \033[0m%s\n",
						 us ? "User mode" : "Supervisor mode");
			StdoutPrintf("  \033[1mRSVD: \033[0m%s\n",
						 rsvd ? "Reserved bit violation" :
								"No reserved bit violation");
			StdoutPrintf("  \033[1mID:   \033[0m%s\n",
						 id ? "Instruction fetch" : "No instruction fetch");
			StdoutPrintf("\n");
		}

		StdoutPrintf("\033[1m\033[34mRegister dump:\033[0m\n");
		StdoutPrintf(
			"  \033[1mrax:\033[0m 0x%.16llx  \033[1mrbx:\033[0m 0x%.16llx  \033[1mrcx:\033[0m 0x%.16llx  \033[1mrdx:\033[0m 0x%.16llx\n",
			frame.rax, frame.rbx, frame.rcx, frame.rdx);
		StdoutPrintf(
			"  \033[1mrsp:\033[0m 0x%.16llx  \033[1mrbp:\033[0m 0x%.16llx  \033[1mrsi:\033[0m 0x%.16llx  \033[1mrdi:\033[0m 0x%.16llx\n",
			frame.rsp, frame.rbp, frame.rsi, frame.rdi);
		StdoutPrintf(
			"  \033[1mr8:\033[0m  0x%.16llx  \033[1mr9:\033[0m  0x%.16llx  \033[1mr10:\033[0m 0x%.16llx  \033[1mr11:\033[0m 0x%.16llx\n",
			frame.r8, frame.r9, frame.r10, frame.r11);
		StdoutPrintf(
			"  \033[1mr12:\033[0m 0x%.16llx  \033[1mr13:\033[0m 0x%.16llx  \033[1mr14:\033[0m 0x%.16llx  \033[1mr15:\033[0m 0x%.16llx\n",
			frame.r12, frame.r13, frame.r14, frame.r15);
		StdoutPrintf(
			"  \033[1mrfl:\033[0m 0x%.16llx  \033[1mrip:\033[0m 0x%.16llx  \033[1mcs:\033[0m  0x%.16llx  \033[1mss:\033[0m  0x%.16llx\n",
			frame.rflags, frame.rip, frame.cs, frame.ss);
		StdoutPrintf(
			"  \033[1mds:\033[0m  0x%.16llx  \033[1mcr2:\033[0m 0x%.16llx  \033[1mcr3:\033[0m 0x%.16llx\n",
			frame.ds, frame.cr2, frame.cr3);

#ifdef _LEAF_DUMP_REG_ON_COM1

		DbgoutPrintf("\n!!! Kernel Panic !!!\n");
		DbgoutPrintf("- %s @ 0x%.16llx \n", exceptionStrings[frame.vector],
					 frame.rip);
		DbgoutPrintf("- Register Dump:\n");
		DbgoutPrintf(
			"  rax: 0x%.16llx, rbx: 0x%.16llx, rcx: 0x%.16llx, rdx: 0x%.16llx\r\n",
			frame.rax, frame.rbx, frame.rcx, frame.rdx);
		DbgoutPrintf(
			"  rsp: 0x%.16llx, rbp: 0x%.16llx, rsi: 0x%.16llx, rdi: 0x%.16llx\r\n",
			frame.rsp, frame.rbp, frame.rsi, frame.rdi);
		DbgoutPrintf(
			"  r8:  0x%.16llx, r9:  0x%.16llx, r10: 0x%.16llx, r11: 0x%.16llx\r\n",
			frame.r8, frame.r9, frame.r10, frame.r11);
		DbgoutPrintf(
			"  r12: 0x%.16llx, r13: 0x%.16llx, r14: 0x%.16llx, r15: 0x%.16llx\r\n",
			frame.r12, frame.r13, frame.r14, frame.r15);
		DbgoutPrintf(
			"  rfl: 0x%.16llx, rip: 0x%.16llx, cs:  0x%.16llx, ss:  0x%.16llx\r\n",
			frame.rflags, frame.rip, frame.cs, frame.ss);
		DbgoutPrintf("  ds:  0x%.16llx, cr2: 0x%.16llx, cr3: 0x%.16llx\r\n",
					 frame.ds, frame.cr2, frame.cr3);
#endif

		HaltAndCatchFire();
	} else if (frame.vector >= 0x20 && frame.vector <= 0x2f) {
		StdoutPrintf("Handeling IRQ %d\n", frame.vector - 0x20);
		unsigned int vector = frame.vector - 0x20;

		if (vector < 16 && irqHandlers[vector]) {
			irqHandler_t handler = irqHandlers[vector];
			handler(&frame);
		} else {
			StdoutPrintf("Unhandled IRQ %d\n", vector);
		}

	} else if (frame.vector == 0x80) {
		// TODO: Hande syscalls
	}
}

void IdtIrqRegister(u8 irq, irqHandler_t handler)
{
	irqHandlers[irq] = handler;
	IOApicRedirectIRQ(irq, 0x20 + irq, 0);
}
void IdtIrqUnregister(u8 irq)
{
	irqHandlers[irq] = NULL;
	IOApicRemoveIRQ(irq, 0);
}