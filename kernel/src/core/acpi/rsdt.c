// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#include <core/acpi/rsdt.h>
#include <kernel.h>
#include <lib/posix/string.h>

#include <core/acpi/madt.h>

xsdtTable *g_xsdt;
rsdtTable *g_rsdt;

int RsdtInitialize()
{
	rsdp_t *rsdp = (rsdp_t *)rsdpResponse->address;
	g_rsdt = (rsdtTable *)(uptr)PHYS_TO_VIRT(rsdp->rsdtAddress);

	if (AcpiUseXsdt()) {
		xsdp_t *xsdp = (xsdp_t *)rsdpResponse->address;
		g_xsdt = (xsdtTable *)(uptr)PHYS_TO_VIRT(xsdp->xsdtAddress);
	}

	madtTable *madt = AcpiFindSdt("APIC");
	if (madt == NULL) {
		// TODO: Better error message.
		return 1;
	}

	MadtInitialize(madt);

	KernelLog("ISO Count: %d\n", g_apicIsosCount);
	for (int i = 0; i < g_apicIsosCount; i++) {
		if (g_apicIsos[i] == NULL) {
			KernelWarnStdout("Failed to initialize ISO %d. Its NULL?\n", i);
			continue;
		}
		KernelLog("Initialized ISO %d\n", i);
	}
	return 0;
}
