// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#include <core/acpi/madt.h>
#include <core/mm/kmalloc.h>

madtTable *g_madtTable;
u64 g_lapicAddress;
u32 g_acpiCpuCount;
apicLapic *g_acpiLapic[CONFIG_CPU_MAX];
apicIOApic **g_acpiIOApic;
u32 g_acpiIOApicCount;
u8 g_acpiCpuIds[CONFIG_CPU_MAX];

u32 g_apicIsosCount;
apicIso *g_apicIsos[16];

int MadtInitialize(madtTable *table)
{
	g_madtTable = table;
	g_acpiCpuCount = 0;
	g_lapicAddress = (u64)PHYS_TO_VIRT(table->lapicAddress);

	u8 *ptr = (u8 *)(table + 1);
	u8 *end = (u8 *)table + table->header.length;

	g_acpiIOApic = (apicIOApic **)KMallocMalloc(sizeof(apicIOApic));
	g_acpiIOApicCount = 0;

	for (usize i = 0; i < 16; i++) {
		g_apicIsos[i] = NULL;
	}

	g_apicIsosCount = 0;

	if (g_acpiIOApic == NULL)
		return 1;

	while (ptr < end) {
		apicHeader *header = (apicHeader *)ptr;

		switch (header->type) {
		case APIC_LAPIC: {
			apicLapic *lapic = (apicLapic *)ptr;
			if (g_acpiCpuCount < CONFIG_CPU_MAX) {
				g_acpiLapic[g_acpiCpuCount] = lapic;
				g_acpiCpuIds[g_acpiCpuCount] = lapic->apicId;
				++g_acpiCpuCount;
			}
			break;
		}
		case APIC_IOAPIC: {
			apicIOApic *ioapic = (apicIOApic *)ptr;
			g_acpiIOApic = (apicIOApic **)KMallocRealloc(
				g_acpiIOApic, sizeof(g_acpiIOApic) + sizeof(ioapic));
			g_acpiIOApic[g_acpiIOApicCount] =
				(apicIOApic *)KMallocMalloc(sizeof(ioapic));
			g_acpiIOApic[g_acpiIOApicCount] = ioapic;
			g_acpiIOApicCount++;
			break;
		}
		case APIC_ISO: {
			apicIso *iso = (apicIso *)ptr;
			if (iso == NULL) {
				KernelError("Invalid ISO at %p\n", ptr);
				return 1;
			}

			g_apicIsos[iso->irq] = iso;

			if (g_apicIsos[iso->irq] == NULL) {
				KernelError("Invalid ISO at %p\n", ptr);
				return 1;
			}
			g_apicIsosCount++;
			KernelLog("ISO %d -> %p\n", g_apicIsosCount, g_apicIsos[iso->irq]);
			break;
		}
		case APIC_IOAPIC_NMI:
		case APIC_LAPIC_NMI:
		case APIC_LAPIC_OVERRIDE:
		case APIC_X2APIC: {
			break;
		}
		default: {
			KernelLog("Found invalid MADT entry %i", header->type);
			break;
		}
		}
		ptr += header->length;
	}
	return 0;
}
