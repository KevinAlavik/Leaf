// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#include <kernel.h>
#include <stddef.h>
#include <stdbool.h>
#include <limine/limine.h>
#include <dev/serial.h>
#define CPU_UTILS_IMPLEMENTATION
#include <cpu/utils.h>
#include <dev/terminal_font.h>
#include <simpterm/st.h>
#include <core/gdt.h>
#include <core/interrupts/idt.h>
#include <lib/std/io/stdout.h>
#include <lib/std/io/dbgout.h>
#include <lib/posix/string.h>
#include <lib/posix/assert.h>
#include <core/mm/pmm.h>
#include <core/mm/vmm.h>
#include <core/mm/heap.h>
#include <core/mm/kmalloc.h>
#include <core/acpi/acpi.h>
#include <core/acpi/madt.h>
#include <core/apic/lapic.h>
#include <core/apic/ioapic.h>
#include <cpu/cpuid.h>

// Limine request thingies
REQUEST_MARKER static volatile LIMINE_BASE_REVISION(2);

REQUEST_MARKER volatile struct limine_framebuffer_request framebufferRequest = {
	.id = LIMINE_FRAMEBUFFER_REQUEST,
	.revision = 0
};

REQUEST_MARKER volatile struct limine_hhdm_request hhdmRequest = {
	.id = LIMINE_HHDM_REQUEST,
	.revision = 0
};

REQUEST_MARKER volatile struct limine_memmap_request memmapRequest = {
	.id = LIMINE_MEMMAP_REQUEST,
	.revision = 0
};

REQUEST_MARKER volatile struct limine_kernel_address_request
	kernelAddressRequest = { .id = LIMINE_KERNEL_ADDRESS_REQUEST,
							 .revision = 0 };

// Used for checking if we are booted into UEFI.
REQUEST_MARKER volatile struct limine_efi_system_table_request
	efiSystemTableRequest = { .id = LIMINE_EFI_SYSTEM_TABLE_REQUEST,
							  .revision = 0 };

REQUEST_MARKER volatile struct limine_rsdp_request rsdpRequest = {
	.id = LIMINE_RSDP_REQUEST,
	.revision = 0
};

REQUEST_START_MARKER static volatile LIMINE_REQUESTS_START_MARKER;
REQUEST_END_MARKER static volatile LIMINE_REQUESTS_END_MARKER;

volatile struct limine_framebuffer *framebufferPointer;
volatile struct limine_memmap_response *memmapResponse;
volatile struct limine_kernel_address_response *kernelAddressResponse;
volatile struct limine_rsdp_response *rsdpResponse;

u64 hhdmOffset;

// Kernel Ctx's
vmmCtx kernelPagemap;
heapCtx *kernelHeap;

// Test IRQ handler
void test(intFrame_t *frame)
{
	StdoutPrintf("a\n");
}

// Kernel entry point
void KernelEntry(void)
{
	if (LIMINE_BASE_REVISION_SUPPORTED == false) {
		HaltAndCatchFire();
	}

	if (framebufferRequest.response == NULL ||
		framebufferRequest.response->framebuffer_count < 1) {
		HaltAndCatchFire();
	}

	framebufferPointer = framebufferRequest.response->framebuffers[0];
	hhdmOffset = hhdmRequest.response->offset;
	memmapResponse = memmapRequest.response;
	kernelAddressResponse = kernelAddressRequest.response;
	rsdpResponse = rsdpRequest.response;

	if (SerialPortInitialize(SERIAL_COM1)) {
		HaltAndCatchFire();
	}

	if (SerialPortInitialize(SERIAL_COM2)) {
		HaltAndCatchFire();
	}

	st_init(framebufferPointer->address, framebufferPointer->width,
			framebufferPointer->height, framebufferPointer->pitch,
			framebufferPointer->bpp, framebufferPointer->red_mask_size,
			framebufferPointer->red_mask_shift,
			framebufferPointer->green_mask_size,
			framebufferPointer->green_mask_shift,
			framebufferPointer->blue_mask_size,
			framebufferPointer->blue_mask_shift, (u32 *)&terminalDefaultFont,
			sizeof(terminalDefaultFont));

	if (GdtInitialize()) {
		SerialPortWriteString(SERIAL_COM2,
							  "Error: Failed to initialize GDT!\n");
		StdoutPrintf("Error: Failed to initialize GDT!\n");
		HaltAndCatchFire();
	}
	KernelLog("Initialized GDT.\n");

	if (IdtInitialize()) {
		SerialPortWriteString(SERIAL_COM2,
							  "Error: Failed to initialize IDT!\n");
		StdoutPrintf("Error: Failed to initialize IDT!\n");
		HaltAndCatchFire();
	}
	KernelLog("Initialized IDT.\n");

	if (PmmInitialize(hhdmOffset)) {
		SerialPortWriteString(SERIAL_COM2,
							  "Error: Failed to initialize PMM!\n");
		StdoutPrintf("Error: Failed to initialize PMM!\n");
		HaltAndCatchFire();
	}

	char *a = (char *)PmmRequestPages(1);
	if (a == NULL) {
		SerialPortWriteString(SERIAL_COM2, "Error: Failed to test PMM!\n");
		StdoutPrintf("Error: Failed to test PMM!\n");
		HaltAndCatchFire();
	}
	PmmFreePages(a, 1);

	KernelLog("Initialized PMM.\n");

	if (VmmInitialize(&kernelPagemap)) {
		SerialPortWriteString(SERIAL_COM2,
							  "Error: Failed to initialize VMM!\n");
		StdoutPrintf("Error: Failed to initialize VMM!\n");
		HaltAndCatchFire();
	}
	KernelLog("Initialized VMM.\n");

	kernelHeap = (heapCtx *)PmmAlloc(sizeof(heapCtx));
	if (kernelHeap == NULL) {
		SerialPortWriteString(
			SERIAL_COM2, "Error: Failed to allocate memory for kernel heap!\n");
		StdoutPrintf("Error: Failed to allocate memory for kernel heap!\n");
		HaltAndCatchFire();
	}

	// TODO: Dont make the heap the entire ammount of RAM.
	if (HeapInitialize(kernelHeap, PmmGetTotal())) {
		SerialPortWriteString(SERIAL_COM2,
							  "Error: Failed to initialize Kernel Heap!\n");
		StdoutPrintf("Error: Failed to initialize Kernel Heap!\n");
		HaltAndCatchFire();
	}
	KernelLog("Initialized Kernel Heap.\n");

	if (KMallocInitialize(kernelHeap)) {
		SerialPortWriteString(SERIAL_COM2,
							  "Error: Failed to initialize Kernel Malloc!\n");
		StdoutPrintf("Error: Failed to initialize Kernel Malloc!\n");
		HaltAndCatchFire();
	}

	if (!CpuidCheckFeature(CPUID_FEAT_EDX_APIC)) {
		SerialPortWriteString(SERIAL_COM2,
							  "Error: CPU does not support APIC!\n");
		StdoutPrintf("Error: CPU does not support APIC!\n");
		HaltAndCatchFire();
	}

	if (AcpiInitialize()) {
		SerialPortWriteString(SERIAL_COM2,
							  "Error: Failed to initialize ACPI!\n");
		StdoutPrintf("Error: Failed to initialize ACPI!\n");
		HaltAndCatchFire();
	}

	KernelLog("Initialized ACPI with %s\n", AcpiUseXsdt() ? "XSDT" : "RSDT");

	if (IOApicInitialize()) {
		SerialPortWriteString(SERIAL_COM2,
							  "Error: Failed to initialize IO APIC!\n");
		StdoutPrintf("Error: Failed to initialize IO APIC!\n");
		HaltAndCatchFire();
	}
	KernelLog("Initialized IO APIC.\n");

	if (LocalApicInitialize()) {
		SerialPortWriteString(SERIAL_COM2,
							  "Error: Failed to initialize Local APIC!\n");
		StdoutPrintf("Error: Failed to initialize Local APIC!\n");
		HaltAndCatchFire();
	}
	KernelLog("Initialized Local APIC.\n");

	StdoutPrintf("\n");

	PRINT_ASCII_BANNER_TO_STDOUT();
	StdoutPrintf("\n");
	StdoutPrintf("\n(%s) Copyright © 2024 Kevin Alavik and contributors\n",
				 _LEAF_KERNEL_ID);

	int rows = g_acpiCpuCount;

	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < rows - i - 1; j++) {
			StdoutPrintf(" ");
		}

		for (int k = 0; k < 2 * i + 1; k++) {
			StdoutPrintf("*");
		}

		StdoutPrintf("\n");
	}
	StdoutPrintf("(Leaf booted with %d cores)\n", g_acpiCpuCount);

	// IdtIrqRegister(1, &test);
	// __asm__ volatile("int $0x20");

	Halt();
}
