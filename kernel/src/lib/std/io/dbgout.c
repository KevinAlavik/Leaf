// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik and ChatGPT :^)

#include <lib/std/io/dbgout.h>
#include <dev/serial.h>
#include <stdarg.h>
#include <stdbool.h>
#include <lib/std/types.h>

void dputc(char character)
{
	SerialPortWrite(SERIAL_COM1, (u8)character);
}

void dputstr(const char *str)
{
	while (*str) {
		dputc(*str++);
	}
}

void dput_int(int num, int width, bool zero_padding, bool left_justify,
			  bool force_sign, bool space_before_positive)
{
	char buffer[12];
	int i = 0;
	bool is_negative = false;

	if (num < 0) {
		is_negative = true;
		num = -num;
	}

	if (num == 0) {
		buffer[i++] = '0';
	} else {
		while (num) {
			buffer[i++] = (num % 10) + '0';
			num /= 10;
		}
	}

	if (is_negative) {
		dputc('-');
		width--;
	} else if (force_sign) {
		dputc('+');
		width--;
	} else if (space_before_positive) {
		dputc(' ');
		width--;
	}

	while (i < width && !left_justify) {
		dputc(zero_padding ? '0' : ' ');
		width--;
	}

	while (i > 0) {
		dputc(buffer[--i]);
	}

	while (i < width && left_justify) {
		dputc(' ');
		width--;
	}
}

void dput_uint(unsigned int num, int width, bool zero_padding,
			   bool left_justify)
{
	char buffer[12];
	int i = 0;

	if (num == 0) {
		buffer[i++] = '0';
	} else {
		while (num) {
			buffer[i++] = (num % 10) + '0';
			num /= 10;
		}
	}

	while (i < width && !left_justify) {
		dputc(zero_padding ? '0' : ' ');
		width--;
	}

	while (i > 0) {
		dputc(buffer[--i]);
	}

	while (i < width && left_justify) {
		dputc(' ');
		width--;
	}
}

void dput_hex(unsigned long long num, bool uppercase, bool alternative_form,
			  int width, bool zero_padding, bool left_justify)
{
	char buffer[17]; // 16 hex digits + null terminator
	int i = 0;

	if (num == 0) {
		buffer[i++] = '0';
	} else {
		while (num) {
			int rem = num % 16;
			if (rem < 10) {
				buffer[i++] = '0' + rem;
			} else {
				buffer[i++] = (uppercase ? 'A' : 'a') + (rem - 10);
			}
			num /= 16;
		}
	}

	if (alternative_form) {
		dputstr("0x");
		width -= 2;
	}

	while (i < 16) {
		buffer[i++] = '0';
	}

	while (i > 0) {
		dputc(buffer[--i]);
	}

	while (i < width && left_justify) {
		dputc(' ');
		width--;
	}
}

void dput_double(double num, int precision, int width, bool left_justify,
				 bool force_sign, bool space_before_positive)
{
	if (num < 0) {
		dputc('-');
		num = -num;
		width--;
	} else if (force_sign) {
		dputc('+');
		width--;
	} else if (space_before_positive) {
		dputc(' ');
		width--;
	}

	long long integer_part = (long long)num;
	double fraction_part = num - integer_part;
	char buffer[12];
	int i = 0;

	if (integer_part == 0) {
		buffer[i++] = '0';
	} else {
		while (integer_part) {
			buffer[i++] = (integer_part % 10) + '0';
			integer_part /= 10;
		}
	}

	while (i < width && !left_justify) {
		dputc(' ');
		width--;
	}

	while (i > 0) {
		dputc(buffer[--i]);
	}

	if (precision > 0) {
		dputc('.');
		while (precision-- > 0) {
			fraction_part *= 10;
			int digit = (int)fraction_part;
			dputc(digit + '0');
			fraction_part -= digit;
		}
	}

	while (i < width && left_justify) {
		dputc(' ');
		width--;
	}
}

void DbgoutPrintf(const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);

	while (*fmt) {
		if (*fmt == '%') {
			fmt++;
			bool left_justify = false;
			bool force_sign = false;
			bool space_before_positive = false;
			bool alternative_form = false;
			bool zero_padding = false;
			int width = 0;
			int precision = -1;
			int length = 0;
			bool uppercase = false;

			// Flags
			while (*fmt == '-' || *fmt == '+' || *fmt == ' ' || *fmt == '#' ||
				   *fmt == '0') {
				if (*fmt == '-')
					left_justify = true;
				if (*fmt == '+')
					force_sign = true;
				if (*fmt == ' ')
					space_before_positive = true;
				if (*fmt == '#')
					alternative_form = true;
				if (*fmt == '0')
					zero_padding = true;
				fmt++;
			}

			// Width
			if (*fmt >= '0' && *fmt <= '9') {
				width = 0;
				while (*fmt >= '0' && *fmt <= '9') {
					width = width * 10 + (*fmt++ - '0');
				}
			} else if (*fmt == '*') {
				width = va_arg(args, int);
				fmt++;
			}

			// Precision
			if (*fmt == '.') {
				fmt++;
				if (*fmt >= '0' && *fmt <= '9') {
					precision = 0;
					while (*fmt >= '0' && *fmt <= '9') {
						precision = precision * 10 + (*fmt++ - '0');
					}
				} else if (*fmt == '*') {
					precision = va_arg(args, int);
					fmt++;
				} else {
					precision = 0;
				}
			}

			// Length specifier
			if (*fmt == 'h') {
				fmt++;
				if (*fmt == 'h') {
					length = 1; // hh
					fmt++;
				} else {
					length = 2; // h
				}
			} else if (*fmt == 'l') {
				fmt++;
				if (*fmt == 'l') {
					length = 4; // ll
					fmt++;
				} else {
					length = 3; // l
				}
			} else if (*fmt == 'j') {
				length = 5; // j
				fmt++;
			} else if (*fmt == 'z') {
				length = 6; // z
				fmt++;
			} else if (*fmt == 't') {
				length = 7; // t
				fmt++;
			} else if (*fmt == 'L') {
				length = 8; // L
				fmt++;
			}

			// Format specifier
			switch (*fmt) {
			case 's': {
				const char *str_arg = va_arg(args, const char *);
				dputstr(str_arg);
				break;
			}
			case 'c': {
				int ch = va_arg(args, int);
				dputc((char)ch);
				break;
			}
			case 'd':
			case 'i': {
				int int_arg;
				if (length == 1) {
					int_arg = (signed char)va_arg(args, int);
				} else if (length == 2) {
					int_arg = (short int)va_arg(args, int);
				} else if (length == 3) {
					int_arg = va_arg(args, long int);
				} else if (length == 4) {
					int_arg = va_arg(args, long long int);
				} else if (length == 5) {
					int_arg = va_arg(args, imax);
				} else if (length == 6) {
					int_arg = va_arg(args, size_t);
				} else if (length == 7) {
					int_arg = va_arg(args, ptrdiff_t);
				} else {
					int_arg = va_arg(args, int);
				}
				dput_int(int_arg, width, zero_padding, left_justify, force_sign,
						 space_before_positive);
				break;
			}
			case 'u': {
				unsigned int uint_arg;
				if (length == 1) {
					uint_arg = (unsigned char)va_arg(args, unsigned int);
				} else if (length == 2) {
					uint_arg = (unsigned short int)va_arg(args, unsigned int);
				} else if (length == 3) {
					uint_arg = va_arg(args, unsigned long int);
				} else if (length == 4) {
					uint_arg = va_arg(args, unsigned long long int);
				} else if (length == 5) {
					uint_arg = va_arg(args, umax);
				} else if (length == 6) {
					uint_arg = va_arg(args, size_t);
				} else if (length == 7) {
					uint_arg = va_arg(args, ptrdiff_t);
				} else {
					uint_arg = va_arg(args, unsigned int);
				}
				dput_uint(uint_arg, width, zero_padding, left_justify);
				break;
			}
			case 'x':
			case 'X': {
				if (*fmt == 'X') {
					uppercase = true;
				}
				unsigned long long hex_arg;
				if (length == 1) {
					hex_arg = (unsigned char)va_arg(args, unsigned int);
				} else if (length == 2) {
					hex_arg = (unsigned short int)va_arg(args, unsigned int);
				} else if (length == 3) {
					hex_arg = va_arg(args, unsigned long int);
				} else if (length == 4) {
					hex_arg = va_arg(args, unsigned long long int);
				} else if (length == 5) {
					hex_arg = va_arg(args, umax);
				} else if (length == 6) {
					hex_arg = va_arg(args, size_t);
				} else if (length == 7) {
					hex_arg = va_arg(args, ptrdiff_t);
				} else {
					hex_arg = va_arg(args, unsigned int);
				}
				dput_hex(hex_arg, uppercase, alternative_form, width,
						 zero_padding, left_justify);
				break;
			}
			case 'f':
			case 'F': {
				double double_arg = va_arg(args, double);
				dput_double(double_arg, precision == -1 ? 6 : precision, width,
							left_justify, force_sign, space_before_positive);
				break;
			}
			case 'p': {
				void *ptr_arg = va_arg(args, void *);
				dput_hex((unsigned long long)ptr_arg, false, true, width,
						 zero_padding, left_justify);
				break;
			}
			case '%': {
				dputc('%');
				break;
			}
			default:
				dputc('%');
				dputc(*fmt);
				break;
			}
		} else {
			dputc(*fmt);
		}
		fmt++;
	}

	va_end(args);
}
