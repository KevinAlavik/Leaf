// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#ifndef ST_CONFIG_H
#define ST_CONFIG_H

#define ST_TAB_WIDTH 4
#define ST_MAX_WIDTH 256
#define ST_MAX_HEIGHT 256
#define ST_SCROLL_TRESHOLD 4

#endif // ST_CONFIG_H