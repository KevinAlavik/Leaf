// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#ifndef CPU_UTILS_H
#define CPU_UTILS_H

#include <lib/std/types.h>
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifdef CPU_UTILS_IMPLEMENTATION

#define CLI() __asm__ volatile("cli" : : : "memory")
#define STI() __asm__ volatile("sti" : : : "memory")

static inline void Halt(void)
{
	for (;;) {
		__asm__ volatile("hlt");
	}
}

static inline void HaltAndCatchFire(void)
{
	CLI();
	Halt();
}

static inline u64 ReadMSR(u32 msr_id)
{
	u32 low, high;
	__asm__ volatile("rdmsr" : "=a"(low), "=d"(high) : "c"(msr_id));
	return ((u64)high << 32) | low;
}

static inline void WriteMSR(u32 msr_id, u64 value)
{
	u32 low = (u32)value;
	u32 high = (u32)(value >> 32);
	__asm__ volatile("wrmsr" : : "a"(low), "d"(high), "c"(msr_id));
}

// Thanks to https://github.com/pdoane/osdev/blob/master/cpu/io.h#L56
static inline void MmioWrite8(void *p, u8 data)
{
	*(volatile u8 *)(p) = data;
}

static inline u8 MmioRead8(void *p)
{
	return *(volatile u8 *)(p);
}

static inline void MmioWrite16(void *p, u16 data)
{
	*(volatile u16 *)(p) = data;
}

static inline u16 MmioRead16(void *p)
{
	return *(volatile u16 *)(p);
}

static inline void MmioWrite32(void *p, u32 data)
{
	*(volatile u32 *)(p) = data;
}

static inline u32 MmioRead32(void *p)
{
	return *(volatile u32 *)(p);
}

static inline void MmioWrite64(void *p, u64 data)
{
	*(volatile u64 *)(p) = data;
}

static inline u64 MmioRead64(void *p)
{
	return *(volatile u64 *)(p);
}

static inline void MmioReadN(void *dst, const volatile void *src, size_t bytes)
{
	volatile u8 *s = (volatile u8 *)src;
	u8 *d = (u8 *)dst;
	while (bytes > 0) {
		*d = *s;
		++s;
		++d;
		--bytes;
	}
}

#endif // CPU_UTILS_IMPLEMENTATION

#ifdef __cplusplus
}
#endif

#endif // CPU_UTILS_H
