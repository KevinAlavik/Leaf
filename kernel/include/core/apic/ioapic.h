// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#ifndef IOAPIC_H
#define IOAPIC_H

#include <core/acpi/acpi.h>
#include <core/acpi/madt.h>
#include <core/apic/lapic.h>

#define SERIAL_IMPLEMENTATION
#include <dev/serial.h>

#define CPU_UTILS_IMPLEMENTATION
#include <cpu/utils.h>

int IOApicInitialize();
void IOApicWrite(apicIOApic *ioapic, uint32_t reg, u32 val);
u32 IOApicRead(apicIOApic *ioapic, u32 reg);
void IOApicRedirectIRQ(u32 irq, u32 vector, u32 id);
void IOApicRemoveIRQ(u32 irq, u32 id);

#endif // IOAPIC_H