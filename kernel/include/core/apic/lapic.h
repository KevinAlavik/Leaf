// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#ifndef LAPIC_H
#define LAPIC_H

#include <core/acpi/acpi.h>
#include <core/acpi/madt.h>
#include <core/apic/lapic.h>

#define SERIAL_IMPLEMENTATION
#include <dev/serial.h>

#define CPU_UTILS_IMPLEMENTATION
#include <cpu/utils.h>

int LocalApicInitialize();

#endif // LAPIC_H