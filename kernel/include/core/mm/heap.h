// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#ifndef HEAP_H
#define HEAP_H

#include <lib/std/types.h>
#include <core/mm/pmm.h>
#include <core/mm/vmm.h>

#define HEAP_CHUNK_SIZE _HEAP_MIN_CHUNK_SIZE

typedef struct heapChunk {
	usize size;
	usize capacity;
	void **data;
} PACKED heapChunk;

typedef struct heapCtx {
	heapChunk **chunks;
	usize size;
	usize maxSize;
	usize chunkCount;
	usize maxChunkCount;
} PACKED heapCtx;

int HeapInitialize(heapCtx *heap, usize maxSize);
void *HeapAlloc(heapCtx *ctx, usize size);
void HeapFree(heapCtx *ctx, void *ptr);
void HeapTestVisualizeChunks(heapCtx *ctx);
void HeapTestVisualizeHeap(heapCtx *ctx);

#endif // HEAP_H
