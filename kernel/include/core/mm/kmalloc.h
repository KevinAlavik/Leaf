// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#ifndef KMALLOC_H
#define KMALLOC_H

#include <lib/std/types.h>
#include <core/mm/pmm.h>
#include <core/mm/vmm.h>
#include <core/mm/heap.h>

int KMallocInitialize(heapCtx *kernelHeap);

void *KMallocMalloc(usize size);
void KMallocFree(void *ptr);
void *KMallocRealloc(void *ptr, usize size);
void *KMallocCalloc(usize nmemb, usize size);

#if _POSIX_KMALLOC
#define kmalloc(size) KMallocMalloc(size)
#define kfree(ptr) KMallocFree(ptr)
#define krealloc(ptr, size) KMallocRealloc(ptr, size)
#define kcalloc(nmemb, size) KMallocCalloc(nmemb, size)
#if _POSIX_KMALLOC_AS_MALLOC
#define malloc(size) KMallocMalloc(size)
#define free(ptr) KMallocFree(ptr)
#define realloc(ptr, size) KMallocRealloc(ptr, size)
#define calloc(nmemb, size) KMallocCalloc(nmemb, size)
#endif // _POSIX_KMALLOC_AS_MALLOC
#else
#define kmalloc(size) void
#define kfree(ptr) void
#define krealloc(ptr, size) void
#define kcalloc(nmemb, size) void
#endif // _POSIX_KMALLOC

#endif // KMALLOC_H
