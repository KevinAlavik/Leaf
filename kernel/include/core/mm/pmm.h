// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#ifndef PMM_H
#define PMM_H

#include <lib/std/types.h>
#include <stddef.h>

#define DIV_ROUND_UP(x, y) (x + (y - 1)) / y
#define ALIGN_UP(x, y) DIV_ROUND_UP(x, y) * y
#define ALIGN_DOWN(x, y) (x / y) * y

#define ALIGN_ADDRESS_UP(ADDR, ALIGN) \
	(u64)((((u64)ADDR + (ALIGN - 1)) / ALIGN) * ALIGN)

int PmmInitialize();
void *PmmRequestPages(size_t numPages);
void PmmFreePages(void *ptr, size_t numPages);

u64 PmmGetTotal();
u64 PmmGetFree();

void *PmmAlloc(usize size);
void *PmmRealloc(void *old, usize size);
void PmmFree(void *pointer);

#endif // PMM_H
