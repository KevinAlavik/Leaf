// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#ifndef VMM_H
#define VMM_H

#include <lib/std/types.h>
#include <core/mm/pmm.h>
#include <stddef.h>

typedef struct {
	u8 present : 1;
	u8 readWrite : 1;
	u8 userSuper : 1;
	u8 writeThrough : 1;
	u8 cacheDisable : 1;
	u8 accessed : 1;
	u8 available0 : 1;
	u8 reserved0 : 1;
	u8 available1 : 4;
	u64 address : 40;
	u16 available2 : 11;
	u8 executeDisable : 1;
} PACKED PML4E;

typedef struct {
	u8 present : 1;
	u8 readWrite : 1;
	u8 userSuper : 1;
	u8 writeThrough : 1;
	u8 cacheDisable : 1;
	u8 accessed : 1;
	u8 available0 : 1;
	u8 pageSize : 1;
	u8 available1 : 4;
	u64 address : 40;
	u16 available2 : 11;
	u8 executeDisable : 1;
} PACKED PML3E;

typedef struct {
	u8 present : 1;
	u8 readWrite : 1;
	u8 userSuper : 1;
	u8 writeThrough : 1;
	u8 cacheDisable : 1;
	u8 accessed : 1;
	u8 available0 : 1;
	u8 pageSize : 1;
	u8 available1 : 4;
	u64 address : 40;
	u16 available2 : 11;
	u8 executeDisable : 1;
} PACKED PML2E;

typedef struct {
	u8 present : 1;
	u8 readWrite : 1;
	u8 userSuper : 1;
	u8 writeThrough : 1;
	u8 cacheDisable : 1;
	u8 accessed : 1;
	u8 dirty : 1;
	u8 PAT : 1;
	u8 global : 1;
	u8 available0 : 3;
	u64 address : 40;
	u8 available1 : 7;
	u8 protKey : 4;
	u8 executeDisable : 1;
} PACKED PML1E;

typedef struct {
	PML4E pml4Entries[512];
} PACKED vmmCtx;

#define VMM_FLAG_PRESENT 1
#define VMM_FLAG_WRITE 2
#define VMM_FLAG_USER 4
#define VMM_FLAG_EXECUTE_DISABLE 0x8000000

int VmmInitialize(vmmCtx *ctx);
void *VmmGetPhysicalAddress(vmmCtx *ctx, u64 virtualAddress);
void VmmMap(vmmCtx *ctx, u64 virtualAddress, u64 physicalAddress, u32 flags);
void VmmMapRange(vmmCtx *ctx, u64 virtualAddressStart, u64 physicalAddress,
				 u64 virtualAddressEnd, u32 flags);
void VmmUnmap(vmmCtx *ctx, u64 virtualAddress);

#endif // VMM_H
