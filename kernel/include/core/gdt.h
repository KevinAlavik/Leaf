// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#ifndef GDT_H
#define GDT_H

#include <lib/std/types.h>
#include <stddef.h>

typedef struct {
	u16 limit;
	uptr base;
} __attribute((packed)) gdtPointer_t;

typedef struct {
	u16 limitLow;
	u16 baseLow;
	u8 baseMid;
	u8 accessByte;
	u8 limitHighAndFlags;
	u8 baseHigh;
} __attribute((packed)) gdtEntry_t;

int GdtInitialize();

extern gdtPointer_t gdtr;

#ifdef __cplusplus
extern "C" {
#endif

#ifdef GDT_IMPLEMENTATION

void GdtFlush(gdtPointer_t gdtr)
{
	__asm__ volatile("mov %0, %%rdi\n"
					 "lgdt (%%rdi)\n"
					 "push $0x8\n"
					 "lea 1f(%%rip), %%rax\n"
					 "push %%rax\n"
					 "lretq\n"
					 "1:\n"
					 "mov $0x10, %%ax\n"
					 "mov %%ax, %%es\n"
					 "mov %%ax, %%ss\n"
					 "mov %%ax, %%gs\n"
					 "mov %%ax, %%ds\n"
					 "mov %%ax, %%fs\n"
					 :
					 : "r"(&gdtr)
					 : "memory");
}

#endif // GDT_IMPLEMENTATION

#ifdef __cplusplus
}
#endif

#endif // GDT_H
