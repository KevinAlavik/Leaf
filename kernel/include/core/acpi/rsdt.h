// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#ifndef RSDT_H
#define RSDT_H

#include <lib/std/types.h>
#include <core/acpi/acpi.h>

typedef struct {
	sdtTable header;
	u32 sdt[];
} PACKED rsdtTable;

typedef struct {
	sdtTable header;
	u64 sdt[];
} PACKED xsdtTable;

extern xsdtTable *g_xsdt;
extern rsdtTable *g_rsdt;

int RsdtInitialize();

#endif // RSDT_H
