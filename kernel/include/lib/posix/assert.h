// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#ifndef ASSERT_H
#define ASSERT_H

#include <lib/std/types.h>
#include <stdbool.h>
#include <lib/std/io/stdout.h>
#define CPU_UTILS_IMPLEMENTATION
#include <cpu/utils.h>

#ifndef _ASSERT_CALLBACK
#ifdef _LEAF_HCF_ASSERT
#if _LEAF_HCF_ASSERT == true
#define _ASSERT_CALLBACK() HaltAndCatchFire()
#else
#define _ASSERT_CALLBACK() \
	do {                   \
	} while (0)
#endif // _LEAF_HCF_ASSERT == true
#else
#define _ASSERT_CALLBACK() \
	do {                   \
	} while (0)
#endif // _LEAF_HCF_ASSERT
#endif // _ASSERT_CALLBACK

#define assert(x)                                                        \
	do {                                                                 \
		if (!(x)) {                                                      \
			StdoutPrintf("%s:%d: %s: Assertion '%s' failed\n", __FILE__, \
						 __LINE__, __func__, #x);                        \
			_ASSERT_CALLBACK();                                          \
		}                                                                \
	} while (0)

#endif // ASSERT_H
