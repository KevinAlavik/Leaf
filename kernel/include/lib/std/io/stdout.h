// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#ifndef STDOUT_H
#define STDOUT_H

#include <lib/std/types.h>
#include <stddef.h>
#include <stdarg.h>
#include <stdbool.h>

void StdoutPrintf(const char *fmt, ...);

#endif // STDOUT_H
