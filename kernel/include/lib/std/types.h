// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#ifndef TYPES_H
#define TYPES_H

#ifndef __SIZE_TYPE__
#define __SIZE_TYPE__ long unsigned int
#endif

#ifndef NULL
#define NULL ((void *)0)
#endif

#ifndef __cplusplus
#ifndef __bool_true_false_are_defined
#else
#undef bool
#undef true
#undef false
#endif

#define bool _Bool
#define true 1
#define false 0
#endif

#ifdef __x86_64__
#endif

typedef signed char s8;
typedef short s16;
typedef int s32;
typedef long long s64;

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;

#ifdef __x86_64__
typedef unsigned long long uptr;
typedef long long imax;
typedef unsigned long long umax;
#else
typedef unsigned int uptr;
typedef int imax;
typedef unsigned int umax;
#endif

#if _TYPES_BIT
typedef bool bit;
#endif
typedef u8 byte;
typedef u16 word;
typedef u32 dword;
typedef u64 qword;

typedef float _Complex f32_complex;
typedef double _Complex f64_complex;

typedef float f32;
typedef double f64;

#if _POSIX_STDINT_TYPES_IN_TYPES

typedef signed char int8_t;
typedef short int16_t;
typedef int int32_t;
typedef long long int64_t;

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long uint64_t;

#define INT8_MIN (-128)
#define INT8_MAX (127)
#define UINT8_MAX (255U)

#define INT16_MIN (-32768)
#define INT16_MAX (32767)
#define UINT16_MAX (65535U)

#define INT32_MIN (-2147483648)
#define INT32_MAX (2147483647)
#define UINT32_MAX (4294967295U)

#define INT64_MIN (-9223372036854775807LL - 1)
#define INT64_MAX (9223372036854775807LL)
#define UINT64_MAX (18446744073709551615ULL)
#endif

typedef __SIZE_TYPE__ usize;

#define S8_MIN (-128)
#define S8_MAX (127)
#define U8_MAX (255U)

#define S16_MIN (-32768)
#define S16_MAX (32767)
#define U16_MAX (65535U)

#define S32_MIN (-2147483648)
#define S32_MAX (2147483647)
#define U32_MAX (4294967295U)

#define S64_MIN (-9223372036854775807LL - 1)
#define S64_MAX (9223372036854775807LL)
#define U64_MAX (18446744073709551615ULL)

#define PACKED __attribute((packed))

#endif // TYPES_H
