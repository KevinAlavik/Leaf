// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#ifndef KERNEL_H
#define KERNEL_H

#include <limine/limine.h>
#include <lib/std/types.h>
#include <core/mm/vmm.h>
#include <lib/std/io/stdout.h>
#include <lib/std/io/dbgout.h>
#include <core/mm/heap.h>

#define CPU_UTILS_IMPLEMENTATION
#include <cpu/utils.h>

#ifndef KERNEL_IMPLEMENTATION

// ---------------------------------------------------------------------------------------------------------------
// Helper defines for limine requests.
// ---------------------------------------------------------------------------------------------------------------

#define REQUEST_START_MARKER __attribute__((section(".requests_start_marker")))

#define REQUEST_END_MARKER __attribute__((section(".requests_end_marker")))

#define REQUEST_MARKER __attribute__((section(".requests")))

// ---------------------------------------------------------------------------------------------------------------
// External defines for requests, responses and wrappers
// ---------------------------------------------------------------------------------------------------------------

// Requests
extern volatile struct limine_framebuffer_request framebufferRequest;
extern volatile struct limine_hhdm_request hhdmRequest;
extern volatile struct limine_memmap_request memmapRequest;
extern volatile struct limine_kernel_address_request kernelAddressRequest;
extern volatile struct limine_rsdp_request rsdpRequest;

// Responses
extern volatile struct limine_framebuffer *framebufferPointer;
extern volatile struct limine_memmap_response *memmapResponse;
extern volatile struct limine_kernel_address_response *kernelAddressResponse;
extern volatile struct limine_rsdp_response *rsdpResponse;

extern u64 hhdmOffset;

// Wrappers
void StdoutPrintf(const char *fmt, ...);

// ---------------------------------------------------------------------------------------------------------------
// Helper defines for macros
// ---------------------------------------------------------------------------------------------------------------

#define PHYS_TO_VIRT(x) ((void *)((u64)(x) + hhdmOffset))
#define VIRT_TO_PHYS(x) ((u64)(x)-hhdmOffset)

#define VoidRedirect(...) ((void)0)

#if _LEAF_MIRROR_KLOG_TO_COM1
#define KernelLog(...)                                           \
	StdoutPrintf("\033[1m%s (%s)\033[0m: ", __func__, __FILE__); \
	StdoutPrintf(__VA_ARGS__);                                   \
	DbgoutPrintf("\033[1m%s (%s)\033[0m: ", __func__, __FILE__); \
	DbgoutPrintf(__VA_ARGS__);
#else
#define KernelLog(...)                                           \
	StdoutPrintf("\033[1m%s (%s)\033[0m: ", __func__, __FILE__); \
	StdoutPrintf(__VA_ARGS__);
#endif

// Place holders until i create some sort or error system.
#define KernelWarn(...)                                                    \
	DbgoutPrintf("\033[1m(Warning) %s (%s)\033[0m: ", __func__, __FILE__); \
	DbgoutPrintf(__VA_ARGS__);

#define KernelWarnStdout(...)                                              \
	StdoutPrintf("\033[1m(Warning) %s (%s)\033[0m: ", __func__, __FILE__); \
	StdoutPrintf(__VA_ARGS__);

#define KernelError(...)                                                 \
	StdoutPrintf("\033[1m(Error) %s (%s)\033[0m: ", __func__, __FILE__); \
	StdoutPrintf(__VA_ARGS__);                                           \
	DbgoutPrintf("\033[1m(Error) %s (%s)\033[0m: ", __func__, __FILE__); \
	DbgoutPrintf(__VA_ARGS__);                                           \
	HaltAndCatchFire();

#ifdef _POSIX_PRINTF
#if _POSIX_PRINTF_MIRROR_TO_COM1
#define printf(...)            \
	StdoutPrintf(__VA_ARGS__); \
	DbgoutPrintf(__VA_ARGS__);
#else
#define printf(...) StdoutPrintf(__VA_ARGS__);
#endif
#else
#define printf(...) VoidRedirect(__VA_ARGS__);
#endif

#define PRINT_ASCII_BANNER_TO_STDOUT()                        \
	StdoutPrintf(" _                __    ___  ____  \n");    \
	StdoutPrintf("| |    ___  __ _ / _|  / _ \\/ ___| \n");   \
	StdoutPrintf("| |   / _ \\/ _` | |_  | | | \\___ \\ \n"); \
	StdoutPrintf("| |__|  __/ (_| |  _| | |_| |___) |\n");    \
	StdoutPrintf("|_____\\___|\\__,_|_|    \\___/|____/ \n");

#define PRINT_ASCII_BANNER_TO_DBGOUT()                        \
	DbgoutPrintf(" _                __    ___  ____  \n");    \
	DbgoutPrintf("| |    ___  __ _ / _|  / _ \\/ ___| \n");   \
	DbgoutPrintf("| |   / _ \\/ _` | |_  | | | \\___ \\ \n"); \
	DbgoutPrintf("| |__|  __/ (_| |  _| | |_| |___) |\n");    \
	DbgoutPrintf("|_____\\___|\\__,_|_|    \\___/|____/ \n");

#define PrintSizeof(x) StdoutPrintf("sizeof(%s): %d\n", #x, sizeof(x));

#define SET_FLAG(num, flag) ((num)->flags |= (flag))
#define CLEAR_FLAG(num, flag) ((num)->flags &= ~(flag))
#define IS_FLAG_SET(num, flag) (((num)->flags & (flag)) != 0)

// ---------------------------------------------------------------------------------------------------------------
// Etc.
// ---------------------------------------------------------------------------------------------------------------
extern u8 __kernel_start;
extern u8 __text_start;
extern u8 __text_end;
extern u8 __rodata_start;
extern u8 __rodata_end;
extern u8 __data_start;
extern u8 __data_end;
extern u8 __bss_start;
extern u8 __bss_end;
extern u8 __kernel_end;

extern vmmCtx kernelPagemap;
extern heapCtx *kernelHeap;

#ifndef PAGE_SIZE
#define PAGE_SIZE 0x1000
#endif

#endif // KERNEL_IMPLEMENTATION

#ifdef KERNEL_IMPLEMENTATION

struct limine_framebuffer *KGetFramebufferPointer()
{
	return framebufferPointer;
}
struct limine_framebuffer_request KGetFramebufferRequest()
{
	return framebufferRequest;
}

void KSetFramebufferPointer(struct limine_framebuffer *newFramebufferPointer)
{
	framebufferPointer = newFramebufferPointer;
}

#endif // KERNEL_IMPLEMENTATION
#endif // KERNEL_H
