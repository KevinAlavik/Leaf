// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#ifndef SERIAL_H
#define SERIAL_H

#include <lib/std/types.h>
#include <stddef.h>

// ---------------------------------------------------------------------------------------------------------------
// Helper defines for COM ports.
// ---------------------------------------------------------------------------------------------------------------

#define SERIAL_COM1 0x3F8
#define SERIAL_COM2 0x2F8
#define SERIAL_COM3 0x3E8
#define SERIAL_COM4 0x2E8
#define SERIAL_COM5 0x5F8
#define SERIAL_COM6 0x4F8
#define SERIAL_COM7 0x5E8
#define SERIAL_COM9 0x4E8

// ---------------------------------------------------------------------------------------------------------------
// Helper functions for serial I/O
// ---------------------------------------------------------------------------------------------------------------

int SerialPortInitialize(u16 port);
int SerialPortReceived(u16 port);
int SerialPortIsTransmitEmpty(u16 port);
u8 SerialPortRead(u16 port);
void SerialPortWrite(u16 port, u8 data);
void SerialPortWriteString(u16 port, const char *data);

#ifdef __cplusplus
extern "C" {
#endif

#ifdef SERIAL_IMPLEMENTATION

// ---------------------------------------------------------------------------------------------------------------
// Serial I/O using in and out instructions
// ---------------------------------------------------------------------------------------------------------------

static inline void SerialIOWriteByte(u16 port, byte data)
{
	__asm__ volatile("outb %b0, %w1" : : "a"(data), "Nd"(port));
}

static inline byte SerialIOReadByte(u16 port)
{
	byte data;
	__asm__ volatile("inb %w1, %b0" : "=a"(data) : "Nd"(port));
	return data;
}

static inline void SerialIOWriteWord(u16 port, word data)
{
	__asm__ volatile("outw %w0, %w1" : : "a"(data), "Nd"(port));
}

static inline word SerialIOReadWord(u16 port)
{
	word data;
	__asm__ volatile("inw %w1, %w0" : "=a"(data) : "Nd"(port));
	return data;
}

static inline void SerialIOWriteDWord(u16 port, dword data)
{
	__asm__ volatile("outl %0, %w1" : : "a"(data), "Nd"(port));
}

static inline dword SerialIOReadDWord(u16 port)
{
	dword data;
	__asm__ volatile("inl %w1, %0" : "=a"(data) : "Nd"(port));
	return data;
}

#endif // SERIAL_IMPLEMENTATION

#ifdef __cplusplus
}
#endif

#endif // SERIAL_H
