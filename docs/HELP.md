# Leaf OS - Help

## 1.0 Build System
To build Leaf you can either manually build it yourself but generally you want to use the pre-provided CLI. Using this CLI you can customize the build behavior easier.

To get started with the CLI simply run:
```bash
make -C build
```

Then run:
```bash
./build/bin/leaf_build
```

Now you should see a little prompt. You can run `help` to get a list of commands