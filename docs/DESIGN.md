# Design of Leaf

This document outlines the general design principles and structure of Leaf.

*Note: This is a work in progress.*

## 1.0 Code Style

This section defines the coding style used in Leaf, inspired by [nakst](https://gitlab.com/nakst) and his project [Essence](https://gitlab.com/nakst/essence).

### 1.1 File Structure

All files in Leaf include the following comment header:

```cpp
// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.
```

This header signifies that the code is part of Leaf, is licensed under MIT, and acknowledges the author, Kevin Alavik. 

Leaf follows these naming conventions:
- PascalCase for functions.
- camelCase for variables.

#### Header Files

Headers in Leaf adopt an approach inspired by stb libraries, using the `XXXX_IMPLEMENTATION` format, for example, `SERIAL_IMPLEMENTATION`.

Headers should be thoroughly documented to facilitate ease of use for programmers. For instance, to use `SerialIOWriteXXXX` functions and other I/O functions:

```cpp
#define SERIAL_IMPLEMENTATION
#include <dev/serial.h>

SerialIOWriteByte(0x3F8, 'A');
```

#### Source Files (Implementation for Headers)

Source files in Leaf also feature the same comment header as the headers. They require less documentation compared to headers. A typical source file structure is as follows:

```cpp
// This file is part of the Leaf OS
// It is released under the MIT license -- see LICENSE
// Written by: Kevin Alavik.

#define SERIAL_IMPLEMENTATION
#include <dev/serial.h>
// Additional implementation code follows
```

### 1.2 Project Structure

Leaf is structured as follows:

- `bios/`: Contains the BIOS file for running Leaf with UEFI.
- `build/`: Houses the build system helper CLI.
- `conf/`: Holds OS configuration files such as `limine.cfg` (bootloader config).
- `docs/`: Contains documentation for Leaf, including design documents and help files.
- `kernel/`: Contains the kernel source code.
  - `kernel/src/`: Source files.
  - `kernel/include/`: Header files.
- `public/`: Source code for the [Leaf website](https://kevinalavik.gitlab.io/Leaf).
- `test/`: Tools for testing and developing the OS.
- `.clang-format`: Configuration file for clang-format.
- `.editorconfig`: Configuration file for editors with [EditorConfig](https://editorconfig.org/) support.
- `.gitignore`: Specifies files and directories to ignore in Git.
- `.gitlab-ci.yml`: Configuration file for website deployment via Gitlab.
- `Brewfile`: Defines packages needed for building Leaf with default settings, for users of [Homebrew](https://brew.sh).
- `CHANGELOG.md`: Changelog for Leaf.
- `CONTRIBUTING.md`: Guidelines for contributing to Leaf.
- `LICENSE`: License file (MIT).
- `Makefile`: Entry point for the build system.
- `README.md`: Project README.

---

This document provides an overview of the structure and coding standards used in Leaf. For more details on contributing or specific aspects of the project, refer to the relevant sections or files mentioned above.