import sys

def file_to_c_array(file_path, array_name):
    with open(file_path, 'rb') as file:
        byte_array = file.read()

    c_array = f"static unsigned char {array_name}[] = {{\n"
    hex_values = ', '.join(f'0x{byte:02X}' for byte in byte_array)
    formatted_hex_values = '\n'.join(hex_values[i:i+60] for i in range(0, len(hex_values), 60))
    c_array += formatted_hex_values
    c_array += "\n};\n"

    return c_array

def save_c_array_to_file(c_array, output_file):
    with open(output_file, 'w') as file:
        file.write(c_array)

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print(f"Usage: {sys.argv[0]} <input_file> <output_file> <array_name>")
        sys.exit(1)

    input_file = sys.argv[1]
    output_file = sys.argv[2]
    array_name = sys.argv[3]

    c_array = file_to_c_array(input_file, array_name)
    save_c_array_to_file(c_array, output_file)
    print(f"C array saved to {output_file}")

